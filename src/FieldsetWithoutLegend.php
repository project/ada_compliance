<?php

/**
 * @file
 * Contains Drupal\ada_compliance\FieldsetWithoutLegend.
 */

namespace Drupal\ada_compliance;

/**
 * Class FieldsetWithoutLegend.
 *
 * @package Drupal\ada_compliance
 */

class FieldsetWithoutLegend {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $fieldsets = $dom->getElementsByTagName('fieldset');
    $foundFieldset= 0;
    foreach ($fieldsets as $fieldset) {
      $legendfound = 0;
      foreach ($fieldset->childNodes as $node) {
        if ($node->nodeName == "legend" and $node->nodeValue != "") $legendfound = 1;
      }
      if (!$legendfound && !$foundFieldset) {
        $errorcode = $dom->saveXML($fieldset, LIBXML_NOEMPTYTAG);
        $result = $ErrorMessage::generateMessage($className, $errorcode, $num, $codes, $texts, $nid);
      }
    }
    return $result;
  }
}