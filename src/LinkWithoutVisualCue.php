<?php

/**
 * @file
 * Contains Drupal\ada_compliance\LinkWithoutVisualCue.
 */

namespace Drupal\ada_compliance;

/**
 * Class LinkWithoutVisualCue.
 *
 * @package Drupal\ada_compliance
 */

class LinkWithoutVisualCue {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $links = $dom->getElementsByTagName('a');
    $link_without_visual_cuefound = 0;
    foreach ($links as $link) {			
      if (isset($link) 
      and (stristr($link->getAttribute('style'), 'text-decoration: none') 
      or stristr($link->getAttribute('style'), 'text-decoration:none'))
      and (!stristr($link->getAttribute('style'), 'font-weight:')
      and !stristr($link->getAttribute('style'), 'font-style: italic') 
      and !stristr($link->getAttribute('style'), 'font-style:italic') 
      and !stristr($link->getAttribute('style'), 'border-bottom:')  
      and !stristr($link->getAttribute('style'), 'border:'))) {	
        $link_without_visual_cue_errorcode = $dom->saveXML($link, LIBXML_NOEMPTYTAG);
        if (!$link_without_visual_cuefound) {
          $result = $ErrorMessage::generateMessage($className, $link_without_visual_cue_errorcode, $num, $codes, $texts, $nid);
        }
      }
    }
    return $result;
  }
}