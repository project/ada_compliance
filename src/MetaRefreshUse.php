<?php

/**
 * @file
 * Contains Drupal\ada_compliance\MetaRefreshUse.
 */

namespace Drupal\ada_compliance;

/**
 * Class MetaRefreshUse.
 *
 * @package Drupal\ada_compliance
 */

class MetaRefreshUse {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $metas = $dom->getElementsByTagName('meta');
    $founderror = 0;
    foreach ($metas as $meta) {
      if (isset($meta) and stristr($meta->getAttribute('http-equiv'), 'refresh') ) {
        $attributes = explode(';',$meta->getAttribute('content'));
        if (((isset($attributes[1]) and stristr($attributes[1],'url=')) 
        and (isset($attributes[0]) and $attributes[0] >= '1'))
        or ((!isset($attributes[1]) or !stristr($attributes[1],'url=')) and $meta->getAttribute('content') >= '0' )) {
          $code =  $dom->saveXML($meta, LIBXML_NOEMPTYTAG);
          if (!$founderror) {
            $result = $ErrorMessage::generateMessage($className, $code, $num, $codes, $texts, $nid);
          }
        }
      }
    }
    return $result;
  }
}