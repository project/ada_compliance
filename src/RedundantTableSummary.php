<?php

/**
 * @file
 * Contains Drupal\ada_compliance\RedundantTableSummary.
 */

namespace Drupal\ada_compliance;

/**
 * Class RedundantTableSummary.
 *
 * @package Drupal\ada_compliance
 */

class RedundantTableSummary {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $tables = $dom->getElementsByTagName('table');
    $foundError = 0;
    foreach ($tables as $table) {
      $tablecode = $dom->saveXML($table, LIBXML_NOEMPTYTAG);
      $summary = $table->getAttribute('summary');
      $captiontext = "";
      $captions = $dom->getElementsByTagName('caption');
      foreach ($captions as $caption) {
        $captiontext = $caption->nodeValue;
      }
      if ($table != "" and $captiontext != "" and $captiontext == $summary ) {
        if (!$foundError) {
          $result = $ErrorMessage::generateMessage($className, $tablecode, $num, $codes, $texts, $nid);
        }
      }
    }
    return $result;
  }
}