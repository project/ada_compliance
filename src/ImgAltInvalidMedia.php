<?php

/**
 * @file
 * Contains Drupal\ada_compliance\ImgAltInvalidMedia.
 */

namespace Drupal\ada_compliance;

/**
 * Class ImgAltInvalidMedia.
 *
 * @package Drupal\ada_compliance
 */

class ImgAltInvalidMedia {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $images = $dom->getElementsByTagName('img');	
    $foundImage = 0;
    foreach ($images as $image) {
      if (isset($image) and $image->hasAttribute('alt') and $image->getAttribute('alt') != "") {
        $imagecode = $dom->saveXML($image, LIBXML_NOEMPTYTAG);
        $alt_text = $image->getAttribute('alt');
        $error = 0;
        for ($i=1; $i < 10; $i++) {
          if (strtolower($alt_text) == 'picture '.$i) $error = 1;
          if (strtolower($alt_text) == 'image '.$i) $error = 1;
          if (strtolower($alt_text) == 'spacer '.$i) $error = 1;
          if (strtolower($alt_text) == '000'.$i) $error = 1;
        }
        if ($error 
          or stristr($alt_text, 'image of')
          or stristr($alt_text, 'graphic of')
          or stristr($alt_text, 'photo of')
          or strtolower($alt_text) == 'picture'
          or strtolower($alt_text) == 'image'
          or strtolower($alt_text) == 'spacer') {
          if (!$foundImage) {
            $result = $ErrorMessage::generateMessage($className, $imagecode, $num, $codes, $texts, $nid);
          }
        }
      }
    }
    return $result;
  }
}