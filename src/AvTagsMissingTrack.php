<?php

/**
 * @file
 * Contains Drupal\ada_compliance\AvTagsMissingTrack.
 */

namespace Drupal\ada_compliance;

/**
 * Class AvTagsMissingTrack.
 *
 * @package Drupal\ada_compliance
 */

class AvTagsMissingTrack {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $iframes = $dom->getElementsByTagName('iframe');
    $foundVideo = 0;
    foreach ($iframes as $iframe) {
      $videocode = $dom->saveXML($iframe, LIBXML_NOEMPTYTAG);
      if (isset($iframe) and (
        // file formats
        strstr($videocode,'.mp4')
        or stristr($videocode,'.m4a')
        or stristr($videocode,'.ogv')
        or stristr($videocode,'.mp3') 
        or stristr($videocode,'.webm') 
        or stristr($videocode,'.flv') 
        or strstr($videocode,'.vtt')
        or strstr($videocode,'.vob')
        or strstr($videocode,'.ogg')
        or strstr($videocode,'.ogv')
        or strstr($videocode,'.wmv')
        or strstr($videocode,'.avi')
        or strstr($videocode,'.m4v')
        or strstr($videocode,'.mov')
        or strstr($videocode,'.swf')
        or strstr($videocode,'.mpeg')
        or strstr($videocode,'.asf')
        or strstr($videocode,'.wav')
        or strstr($videocode,'.wma')
        or strstr($videocode,'.mid')
        or strstr($videocode,'.midi')
        or strstr($videocode,'.au')
        or strstr($videocode,'.aiff')
        or strstr($videocode,'.qt')
        // service providers
        or strstr($videocode,'www.youtube.com') 
        or strstr($videocode,'vimeo.com')
        or strstr($videocode,'videopress.com')
        or strstr($videocode,'embed.ted.com')
        or strstr($videocode,'hulu.com')
        or strstr($videocode,'wordpress.tv')
        or strstr($videocode,'animoto.com')
        or strstr($videocode,'blip.com')
        or strstr($videocode,'vine.com')
        or strstr($videocode,'collegehumor.com')
        or strstr($videocode,'dailymotion.com')
        or strstr($videocode,'funnyordie.com')
        or strstr($videocode,'mixcloud.com')
        or strstr($videocode,'reverbnation.com')
        or strstr($videocode,'soundcloud.com')
        or strstr($videocode,'spotify.com'))) {
          if (!$foundVideo) {
            $result = $ErrorMessage::generateMessage($className, 
                      $videocode, $num, $codes, $texts, $nid);
          }
      }
    }
    $videos = $dom->getElementsByTagName('video');
    foreach ($videos as $video) {
      $videocode = $dom->saveXML($video, LIBXML_NOEMPTYTAG);
      if (isset($video) and !strstr($videocode,'<track')) {
        if (!$foundVideo) {
          $result = $ErrorMessage::generateMessage($className, 
                    $videocode, $num, $codes, $texts, $nid);
        }
      }
    }
	
    $audios = $dom->getElementsByTagName('audio');
    foreach ($audios as $audio) {
      $audiocode = $dom->saveXML($audio, LIBXML_NOEMPTYTAG);
      if (isset($audio) and !strstr($audiocode,'<track')) {
        if (!$foundVideo) {
          $result = $ErrorMessage::generateMessage($className, 
                    $audiocode, $num, $codes, $texts, $nid);
        }
      }
    }
    return $result;
  }
}