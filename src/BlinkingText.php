<?php

/**
 * @file
 * Contains Drupal\ada_compliance\BlinkingText.
 */

namespace Drupal\ada_compliance;

/**
 * Class BlinkingText.
 *
 * @package Drupal\ada_compliance
 */

class BlinkingText {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $blinks = $dom->getElementsByTagName('blink');
    $blinking_textfound = 0;
    foreach ($blinks as $blink) {
      $blinking_text_errorcode = $dom->saveXML($blink, LIBXML_NOEMPTYTAG);
      if (!$blinking_textfound) {
        $result = $ErrorMessage::generateMessage($className, 
                  $blinking_text_errorcode, $num, $codes, 
                  $texts, $nid);
      }
    }
    $pattern = "|text\-decoration:\s*blink|i";
    if (preg_match_all($pattern, $content, $matches,PREG_OFFSET_CAPTURE)) {
      $matchsize = sizeof($matches[0]);
      for ($i=0; $i < $matchsize; $i++) {
        if (isset($matches[0][$i]) and $matches[0][$i][0] != "") {
          $blinking_text_errorcode = htmlspecialchars($matches[0][$i][0]).
                                     ' (char #: '.$matches[0][$i][1].')';
          if (!$blinking_textfound) {
            $result = $ErrorMessage::generateMessage($className, 
                      $blinking_text_errorcode, $num, $codes, 
                      $texts, $nid);
          }
        }
      }
    }
    return $result;
  }
}