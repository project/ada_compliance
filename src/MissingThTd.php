<?php

/**
 * @file
 * Contains Drupal\ada_compliance\MissingThTd.
 */

namespace Drupal\ada_compliance;

/**
 * Class MissingThTd.
 *
 * @package Drupal\ada_compliance
 */

class MissingThTd {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $tables = $dom->getElementsByTagName('table');
    $k = 0;
    $foundmissingTHid = 0;
    foreach ($tables as $table) {
      $tablecode = $dom->saveXML($table, LIBXML_NOEMPTYTAG);
      if ($table != "" and stristr($tablecode, "<th")) {
        $tablecells = $tables->item($k)->getElementsByTagName('td');
        $additionalerrorInfo =  "Missing ID: ";
        foreach ($tablecells as $td) {
          if (isset($td) and $td->getAttribute('header') != "") {
            $headerids = explode(" ",$td->getAttribute('header'));
            foreach ($headerids as $headeridvalue) {
              if (!stristr($tablecode, 'id="'.$headeridvalue.'"') and !stristr($tablecode, "id='".$headeridvalue."'")) {
                if (!strstr($additionalerrorInfo, $headeridvalue)) $additionalerrorInfo .= $headeridvalue." ";
              }
            }
          }
        }
        if (isset($additionalerrorInfo) and $additionalerrorInfo != "Missing ID: ") {
          $additionalerrorInfo .= $tablecode;
          if (!$foundmissingTHid) {
            $result = $ErrorMessage::generateMessage($className, $tablecode, $num, $codes, $texts, $nid);
          }
        }
      }
      $k++;
    }
    return $result;
  }
}