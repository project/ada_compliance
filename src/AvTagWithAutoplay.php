<?php

/**
 * @file
 * Contains Drupal\ada_compliance\AvTagWithAutoplay.
 */

namespace Drupal\ada_compliance;

/**
 * Class AvTagWithAutoplay.
 *
 * @package Drupal\ada_compliance
 */

class AvTagWithAutoplay {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $videos = $dom->getElementsByTagName('video');
    foreach ($videos as $video) {
      $videocode = $dom->saveXML($video, LIBXML_NOEMPTYTAG);
      if (isset($video) and (stristr($videocode,'autoplay') and 
      !stristr($videocode,'muted'))) {
        if (!$foundAutoplay) {
          $result = $ErrorMessage::generateMessage($className, 
                    $videocode, $num, $codes, $texts, $nid);
        }
      }
    }	
    $audios = $dom->getElementsByTagName('audio');
    foreach ($audios as $audio) {
      $audiocode = $dom->saveXML($audio, LIBXML_NOEMPTYTAG);
      if (isset($audio) and (strstr($audiocode,'autoplay') and 
      !strstr($audiocode,'muted'))) {
        if (!$foundAutoplay) {
          $result = $ErrorMessage::generateMessage($className, 
                    $audiocode, $num, $codes, $texts, $nid);
        }
      }
    }
    $objects = $dom->getElementsByTagName('object');
    foreach ($objects as $object) {
      $nodes = $object->childNodes;
      foreach ($nodes as $node) {
        if ($node->nodeName == "param") {
          if ((strtolower($node->getAttribute('name')) == "autoplay" 
             and strtolower($node->getAttribute('value')) == "true") or 
             (strtolower($node->getAttribute('name')) == "flashvars" 
             and stristr($node->getAttribute('value'),"autoPlay=true"))) {
               $objectcode = $dom->saveXML($object, LIBXML_NOEMPTYTAG);
               if (!$foundAutoplay) {
                 $result = $ErrorMessage::generateMessage($className, 
                           $objectcode, $num, $codes, $texts, $nid);
               }
          }
        }
      }
    }

    $embeds = $dom->getElementsByTagName('embed');
    foreach ($embeds as $embed) {
      $embedcode = $dom->saveXML($embed, LIBXML_NOEMPTYTAG);
      if (isset($embed) and $embed->getAttribute('autostart') == 'true') {
        if (!$foundAutoplay) {
          $result = $ErrorMessage::generateMessage($className, 
                    $embedcode, $num, $codes, $texts, $nid);
        }
      }
    }
    return $result;
  }
}