<?php

/**
 * @file
 * Contains Drupal\ada_compliance\BackgroundImage.
 */

namespace Drupal\ada_compliance;

/**
 * Class BackgroundImage.
 *
 * @package Drupal\ada_compliance
 */

class BackgroundImage {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = '';
    $elements = $dom->getElementsByTagName('*');
    $background_imagefound = 0;
    foreach ($elements as $element) {
      if (isset($element) 
        and (stristr($element->getAttribute('style'), 'background:') 
        or stristr($element->getAttribute('style'), 'background-image:') )
        and stristr($element->getAttribute('style'),'url(')) {
          $background_image_errorcode = 
            $dom->saveXML($element, LIBXML_NOEMPTYTAG);
          if (!$background_imagefound) {
            $result = $ErrorMessage::generateMessage($className, 
                      $background_image_errorcode, $num, $codes, 
                      $texts, $nid);
          }
      }
    }
    return $result;
  }
}