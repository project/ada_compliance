<?php

/**
 * @file
 * Contains Drupal\ada_compliance\ImgAltFilename.
 */

namespace Drupal\ada_compliance;

/**
 * Class ImgAltFilename.
 *
 * @package Drupal\ada_compliance
 */

class ImgAltFilename {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $images = $dom->getElementsByTagName('img');
    $foundInvalidalt = 0;
    foreach ($images as $image) {
      if (isset($image) and $image->hasAttribute('alt')) {
        $alt_text = $image->getAttribute('alt');
        if (!$foundInvalidalt && (stristr($alt_text, '.jpg') 
          or stristr($alt_text, '.png') 
          or stristr($alt_text, '.gif')  
          or stristr($alt_text, '_'))) {
            $imagecode = $dom->saveXML($image, LIBXML_NOEMPTYTAG);
            $result = $ErrorMessage::generateMessage($className, $imagecode, $num, $codes, $texts, $nid);
        }
      }
    }
    return $result;
  }
}