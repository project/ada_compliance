<?php

/**
 * @file
 * Contains Drupal\ada_compliance\ContextChangeForm.
 */

namespace Drupal\ada_compliance;

/**
 * Class ContextChangeForm.
 *
 * @package Drupal\ada_compliance
 */

class ContextChangeForm {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $forms = $dom->getElementsByTagName('form');
    $k = 0;
    foreach ($forms as $form) {
      $fields = $forms->item($k)->getElementsByTagName('input');
      $selects = $forms->item($k)->getElementsByTagName('select');
      $textareas = $forms->item($k)->getElementsByTagName('textarea');
      $formAtttributestoIgnore[] = 'submit';
      $formAtttributestoIgnore[] = 'hidden';	
      $foundField = 0;
      // check input fields
      foreach ($fields as $field) {
        $i = 0;   
        // ignore 	
        if (isset($field)) {
          if (
          ($field->getAttribute('onclick') 
          and (stristr($field->getAttribute('onclick'),'window.open') 
          or stristr($field->getAttribute('onclick'),'.submit'))) 
          or ($field->getAttribute('onchange') 
          and (stristr($field->getAttribute('onchange'),'window.open') 
          or stristr($field->getAttribute('onchange'),'.submit')))
          or ($field->getAttribute('onfocus') 
          and (stristr($field->getAttribute('onfocus'),'window.open') 
          or stristr($field->getAttribute('onfocus'),'.submit')))
          or ($field->getAttribute('onkeydown') 
          and (stristr($field->getAttribute('onkeydown'),'window.open') 
          or stristr($field->getAttribute('onkeydown'),'.submit'))) 		
          or ($field->getAttribute('onkeypress') 
          and (stristr($field->getAttribute('onkeypress'),'window.open') 
          or stristr($field->getAttribute('onkeypress'),'.submit')))) {
            $formfieldcode = $dom->saveXML($field, LIBXML_NOEMPTYTAG);
            if (!$foundField) {
              $result = $ErrorMessage::generateMessage($className, $formfieldcode, $num, $codes, $texts, $nid);			
            }
          }
          $i++;
        }
      }

      // check select fields
      foreach ($selects as $field) {
        $i = 0;   
        if (
        ($field->getAttribute('onclick') 
        and (stristr($field->getAttribute('onclick'),'window.open') 
        or stristr($field->getAttribute('onclick'),'.submit'))) 
        or ($field->getAttribute('onchange') 
        and (stristr($field->getAttribute('onchange'),'window.open') 
        or stristr($field->getAttribute('onchange'),'.submit'))) 
        or ($field->getAttribute('onfocus') 
        and (stristr($field->getAttribute('onfocus'),'window.open') 
        or stristr($field->getAttribute('onfocus'),'.submit')))	
        or ($field->getAttribute('onkeydown') 
        and (stristr($field->getAttribute('onkeydown'),'window.open') 
        or stristr($field->getAttribute('onkeydown'),'.submit'))) 	
        or ($field->getAttribute('onkeypress') 
        and (stristr($field->getAttribute('onkeypress'),'window.open') 
        or stristr($field->getAttribute('onkeypress'),'.submit')))) {
          $formfieldcode = $dom->saveXML($field, LIBXML_NOEMPTYTAG);
          // display error
          if (!$foundField) {
            $result = $ErrorMessage::generateMessage($className, $formfieldcode, $num, $codes, $texts, $nid);
            //$foundField= 1;
          }
        }
        $i++;
      }	

      // check textareas fields
      foreach ($textareas as $field) {
        $i = 0;   
        if (
        ($field->getAttribute('onclick') 
        and (stristr($field->getAttribute('onclick'),'window.open') 
        or stristr($field->getAttribute('onclick'),'.submit'))) 
        or ($field->getAttribute('onchange') 
        and (stristr($field->getAttribute('onchange'),'window.open') 
        or stristr($field->getAttribute('onchange'),'.submit'))) 
        or ($field->getAttribute('onfocus') 
        and (stristr($field->getAttribute('onfocus'),'window.open') 
        or stristr($field->getAttribute('onfocus'),'.submit')))	
        or ($field->getAttribute('onkeydown') 
        and (stristr($field->getAttribute('onkeydown'),'window.open') 
        or stristr($field->getAttribute('onkeydown'),'.submit'))) 	
        or ($field->getAttribute('onkeypress') 
        and (stristr($field->getAttribute('onkeypress'),'window.open') 
        or stristr($field->getAttribute('onkeypress'),'.submit')))) {
          $formfieldcode = $dom->saveXML($field, LIBXML_NOEMPTYTAG);
          if (!$foundField) {
            $result = $ErrorMessage::generateMessage($className, $formfieldcode, $num, $codes, $texts, $nid);
          }
        }
        $i++;
      }	
      $k++;
    }
    return $result;
  }
}