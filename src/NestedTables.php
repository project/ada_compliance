<?php

/**
 * @file
 * Contains Drupal\ada_compliance\NestedTables.
 */

namespace Drupal\ada_compliance;

/**
 * Class NestedTables.
 *
 * @package Drupal\ada_compliance
 */

class NestedTables {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $tables = $dom->getElementsByTagName('table');
    $errorfound = 0;
    foreach ($tables as $table) {
      $tablecode = $dom->saveXML($table, LIBXML_NOEMPTYTAG);
      if ($table != "" and $table->getElementsByTagName('table')->length > 0) {
        $tableinner = $table->getElementsByTagName('table');
        foreach ($tableinner as $tablechild) {
          if (!$errorfound) {
            $result = $ErrorMessage::generateMessage($className, $tablecode, $num, $codes, $texts, $nid);
          }
        }
      }
    }
    return $result;
  }
}