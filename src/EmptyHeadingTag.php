<?php

/**
 * @file
 * Contains Drupal\ada_compliance\EmptyHeadingTag.
 */

namespace Drupal\ada_compliance;

/**
 * Class EmptyHeadingTag.
 *
 * @package Drupal\ada_compliance
 */

class EmptyHeadingTag {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $foundemptytag = 0;
    for ($i = 1; $i <= 6; $i++) {
      $headings = $dom->getElementsByTagName('h'.$i);
      foreach ($headings as $heading) {
        $headingcode = $dom->saveXML($heading, LIBXML_NOEMPTYTAG);
        if (str_ireplace(array(' ','&nbsp;','-','_'),'',htmlentities(strip_tags($heading->nodeValue))) == "") {
          if (!$foundemptytag) {
            $result = $ErrorMessage::generateMessage($className, $headingcode, $num, $codes, $texts, $nid);
          }
        }
      }
    }
    return $result;
  }
}