<?php

/**
 * @file
 * Contains Drupal\ada_compliance\ErrorMessage.
 */

namespace Drupal\ada_compliance;

/**
 * Class ErrorMessage.
 *
 * @package Drupal\ada_compliance
 */

class ErrorMessage {

  static function generateMessage($class, $htmlcode, &$num, &$codes, $texts, $nid) {
    $num++;
    array_push($codes, array($htmlcode, '<a name="'.$nid.'_'.$num.'_'.$texts[$class]['category'].'" id="'.$nid.'_'.$num.'_'.$texts[$class]['category'].'"></a><strong>'.$texts[$class]['category'].'.</strong> '.$texts[$class]['error_title'],$texts[$class]['error_description'].($texts[$class]['wga_link']?'<br><a href="'.$texts[$class]['wga_link'].'" target="_blank" class="adaNewWindowInfo">'.$texts[$class]['wga_level'].'</a>':''),$texts[$class]['level'],$texts[$class]['category']));
    $result = '<p>';
    $extra_warning = '';
    if ($texts[$class]['level']!='normal') {
      $extra_warning = '<span class="'.$texts[$class]['level'].'_level">'.strtoupper($texts[$class]['level']).'!</span> ';
    }
    $result .= $num.'. '.$extra_warning.$texts[$class]['error_description'];
    if ($texts[$class]['wga_link']) {
      $result .= '<br><a href="'.$texts[$class]['wga_link'].'" target="_blank" class="adaNewWindowInfo">'.$texts[$class]['wga_level'].'</a>';
    }
    $result .= '</p>';
    return $result;
  }

  static function DOMRemoveChild(DOMNode $parentNode) {
    $newdoc = new DOMDocument();
    libxml_use_internal_errors(true);
    $newdoc->loadHTML("<ada></ada>");	
    $cloned = $parentNode->cloneNode(TRUE);
    while ($cloned->hasChildNodes()) {
      $cloned->removeChild($cloned->firstChild);
    } 
    $newdoc->appendChild($newdoc->importNode($cloned,TRUE));	
    $error = preg_replace('/^<!DOCTYPE.+?>/', '', str_replace( array('<html>', '</html>', '<body>', '</body>','<ada>', '</ada>'), array('', '', '', '','',''), $newdoc->saveHTML()));
    return $error;
  }

  static function ignore_inside_valid_caption($imagecode, $content) {
    $dom = new \DOMDocument;
    libxml_use_internal_errors(true);
    $dom->loadHTML($content);	
    $figures = $dom->getElementsByTagName('figure');
    $k = 0;
    foreach ($figures as $figure) {	
      $images = $figures->item($k)->getElementsByTagName('img');
      foreach ($images as $image) {	
        if (strstr($imagecode, $image->getAttribute('src')) and $figure->nodeValue != "") {
          return 1;
        }
      }
      $k++;
    }
    $divs = $dom->getElementsByTagName('div');
    foreach ($divs as $div) {	
      if (stristr($div->getAttribute('class'), 'wp-caption')) {
        $images = $div->getElementsByTagName('img');
        foreach ($images as $image) {	
          if (strstr($imagecode, $image->getAttribute('src')) and strlen($div->nodeValue) > 5) {
            return 1; 
          }
        }
      }
    }	
    return 0;
  }

  static function GETinnerHTML(\DOMElement $element) {
    $doc = $element->ownerDocument;
    $html = '';
    foreach ($element->childNodes as $node) {
      $html .= $doc->saveHTML($node);
    }
    return $html;
  }
}
