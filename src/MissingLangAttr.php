<?php

/**
 * @file
 * Contains Drupal\ada_compliance\MissingLangAttr.
 */

namespace Drupal\ada_compliance;

/**
 * Class MissingLangAttr.
 *
 * @package Drupal\ada_compliance
 */

class MissingLangAttr {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $html = $dom->getElementsByTagName('html');
    foreach ($html as $htmlcode) {	
      if ($htmlcode->getAttribute('lang') != "" or $htmlcode->getAttribute('xml:lang') != "") continue;
      $code = substr($dom->saveXML($htmlcode, LIBXML_NOEMPTYTAG), 0, strpos($dom->saveXML($htmlcode, LIBXML_NOEMPTYTAG), '>')+1);
      $result = $ErrorMessage::generateMessage($className, $code, $num, $codes, $texts, $nid);
    }
    return $result;
  }
}