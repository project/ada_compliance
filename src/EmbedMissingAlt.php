<?php

/**
 * @file
 * Contains Drupal\ada_compliance\EmbedMissingAlt.
 */

namespace Drupal\ada_compliance;

/**
 * Class EmbedMissingAlt.
 *
 * @package Drupal\ada_compliance
 */

class EmbedMissingAlt {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
      $result = "";

      $embeds = $dom->getElementsByTagName('embed');
      $foundmissingnoembed = 0;
      foreach ($embeds as $embed) {
			
          $matchfound = 0;
	
          $childnodes = $embed->childNodes;
          foreach ($childnodes as $childnode) {
              if($childnode->nodeName == "noembed" and $childnode->nodeValue != "") {
                  $matchfound = 1;	
              }
          }
	
          if (isset($embed) and $matchfound == 0) {
              $embedcode = $dom->saveXML($embed, LIBXML_NOEMPTYTAG);	
              if(!$foundmissingnoembed) {
                  //$foundmissingnoembed = 1;
                  $result = $ErrorMessage::generateMessage($className, $embedcode, $num, $codes, $texts, $nid);
              }
          }

      }

      return $result;

    }

}