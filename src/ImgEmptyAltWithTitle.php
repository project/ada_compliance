<?php

/**
 * @file
 * Contains Drupal\ada_compliance\ImgEmptyAltWithTitle.
 */

namespace Drupal\ada_compliance;

/**
 * Class ImgEmptyAltWithTitle.
 *
 * @package Drupal\ada_compliance
 */

class ImgEmptyAltWithTitle {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $images = $dom->getElementsByTagName('img');
    $foundImage = 0;
    foreach ($images as $image) {
      if (isset($image) and ($image->hasAttribute('alt') and $image->getAttribute('alt') == "") and ($image->getAttribute('title') != "" or $image->getAttribute('aria-label') != "")) {
        $imagecode = $dom->saveXML($image, LIBXML_NOEMPTYTAG);
        if (!$foundImage) {
          $result = $ErrorMessage::generateMessage($className, $imagecode, $num, $codes, $texts, $nid);
        }
      }
    }
    return $result;
  }
}