<?php

/**
 * @file
 * Contains Drupal\ada_compliance\MissingTd.
 */

namespace Drupal\ada_compliance;

/**
 * Class MissingTd.
 *
 * @package Drupal\ada_compliance
 */

class MissingTd {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $tables = $dom->getElementsByTagName('table');
    $result = '';
    foreach ($tables as $table) {
      $tablecode = $dom->saveXML($table, LIBXML_NOEMPTYTAG);
      if ($table != "" and !stristr($tablecode, "<td") ) {
        if (!isset($foundTD)) {
          $result = $ErrorMessage::generateMessage($className, $tablecode, $num, $codes, $texts, $nid);
        }
      }
    }
    return $result;
  }
}