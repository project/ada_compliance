<?php

/**
 * @file
 * Contains Drupal\ada_compliance\Texts.
 */

namespace Drupal\ada_compliance;

/**
 * Class Texts.
 *
 * @package Drupal\ada_compliance
 */

class Texts {
  static function getTexts() {
    $lang['UnmarkedRequiredFields']['category'] = '2.4.6.ARIA2';
    $lang['UnmarkedRequiredFields']['wga_level'] = 'WCAG 2.1 (Level A) - 2.4.6';
    $lang['UnmarkedRequiredFields']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#headings-and-labels';
    $lang['UnmarkedRequiredFields']['level'] = 'alert';
    $lang['UnmarkedRequiredFields']['error_title'] = 'Forms not clearly identifying required fields.';
    $lang['UnmarkedRequiredFields']['error_description'] = '';
    $lang['UnmarkedRequiredFields']['wga_description'] = '';

    $lang['ComplexTables']['category'] = '1.3.2.F49';
    $lang['ComplexTables']['wga_level'] = 'WCAG 2.1 (Level A) - 1.3.2';
    $lang['ComplexTables']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#meaningful-sequence';
    $lang['ComplexTables']['level'] = 'alert';
    $lang['ComplexTables']['error_title'] = 'Complex layout for tables that would not be easily understood by screen reader user.';
    $lang['ComplexTables']['error_description'] = '';
    $lang['ComplexTables']['wga_description'] = '';

    $lang['TabOrderModified']['category'] = '2.4.3.F44';
    $lang['TabOrderModified']['wga_level'] = 'WCAG 2.1 (Level A) - 2.4.3';
    $lang['TabOrderModified']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#focus-order';
    $lang['TabOrderModified']['level'] = 'alert';
    $lang['TabOrderModified']['error_title'] = 'Tabindex attribute changes tab order.';
    $lang['TabOrderModified']['error_description'] = '';
    $lang['TabOrderModified']['wga_description'] = '';

    $lang['ImgMissingAltMedia']['category'] = '1.1.1.H37';
    $lang['ImgMissingAltMedia']['wga_level'] = 'WCAG 2.1 (Level A) - 1.1.1';
    $lang['ImgMissingAltMedia']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#non-text-content';
    $lang['ImgMissingAltMedia']['level'] = 'alert';
    $lang['ImgMissingAltMedia']['error_title'] = 'Image without ALT text.';
    $lang['ImgMissingAltMedia']['error_description'] = '';
    $lang['ImgMissingAltMedia']['wga_description'] = '';

    $lang['BackgroundImage']['category'] = '1.1.1.F3';
    $lang['BackgroundImage']['wga_level'] = 'WCAG 2.1 (Level A) - 1.1.1';
    $lang['BackgroundImage']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#non-text-content';
    $lang['BackgroundImage']['level'] = 'alert';
    $lang['BackgroundImage']['error_title'] = 'Images added using css property background-image.';
    $lang['BackgroundImage']['error_description'] = '';
    $lang['BackgroundImage']['wga_description'] = '';

    $lang['ImgAltFilename']['category'] = '1.1.1.H37.2';
    $lang['ImgAltFilename']['wga_level'] = 'WCAG 2.1 (Level A) - 1.1.1';
    $lang['ImgAltFilename']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#non-text-content';
    $lang['ImgAltFilename']['level'] = 'alert';
    $lang['ImgAltFilename']['error_title'] = 'Image ALT text includes image filename, not helpful to screen reader user in understanding page content.';
    $lang['ImgAltFilename']['error_description'] = '';
    $lang['ImgAltFilename']['wga_description'] = '';

    $lang['RedundantTableSummary']['category'] = '1.3.1.H39';
    $lang['RedundantTableSummary']['wga_level'] = 'WCAG 2.1 (Level A) - 1.3.1';
    $lang['RedundantTableSummary']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#info-and-relationships';
    $lang['RedundantTableSummary']['level'] = 'alert';
    $lang['RedundantTableSummary']['error_title'] = 'Table\'s caption tag content duplicated in summary attribute.';
    $lang['RedundantTableSummary']['error_description'] = '';
    $lang['RedundantTableSummary']['wga_description'] = '';

    $lang['RedundantTitleTag']['category'] = '2.4.4.H33';
    $lang['RedundantTitleTag']['wga_level'] = 'WCAG 2.1 (Level A) - 2.4.4';
    $lang['RedundantTitleTag']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#link-purpose-in-context';
    $lang['RedundantTitleTag']['level'] = 'alert';
    $lang['RedundantTitleTag']['error_title'] = 'Content within body of a link duplicated in anchor tag\'s title.';
    $lang['RedundantTitleTag']['error_description'] = '';
    $lang['RedundantTitleTag']['wga_description'] = '';

    $lang['ImgLinkedToSelf']['category'] = '1.1.1.H37.3';
    $lang['ImgLinkedToSelf']['wga_level'] = 'WCAG 2.1 (Level A) - 1.1.1';
    $lang['ImgLinkedToSelf']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#non-text-content';
    $lang['ImgLinkedToSelf']['level'] = 'alert';
    $lang['ImgLinkedToSelf']['error_title'] = 'Images refer to itself.';
    $lang['ImgLinkedToSelf']['error_description'] = '';
    $lang['ImgLinkedToSelf']['wga_description'] = '';

    $lang['ImgAltMarkedPresentation']['category'] = '1.1.1.WAI-ARIA';
    $lang['ImgAltMarkedPresentation']['wga_level'] = 'WCAG 2.1 (Level A) - 1.1.1';
    $lang['ImgAltMarkedPresentation']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#non-text-content';
    $lang['ImgAltMarkedPresentation']['level'] = 'alert';
    $lang['ImgAltMarkedPresentation']['error_title'] = 'Decorative images with ALT text, not helpful to screen reader user in understanding page content.';
    $lang['ImgAltMarkedPresentation']['error_description'] = '';
    $lang['ImgAltMarkedPresentation']['wga_description'] = '';

    $lang['MissingHeadings']['category'] = '1.3.1.H42';
    $lang['MissingHeadings']['wga_level'] = 'WCAG 2.1 (Level A) - 1.3.1';
    $lang['MissingHeadings']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#info-and-relationships';
    $lang['MissingHeadings']['level'] = 'alert';
    $lang['MissingHeadings']['error_title'] = 'Page content structure not defined using heading tags.';
    $lang['MissingHeadings']['error_description'] = '';
    $lang['MissingHeadings']['wga_description'] = '';

    $lang['ImgEmptyAlt']['category'] = '1.1.1.2';
    $lang['ImgEmptyAlt']['wga_level'] = 'WCAG 2.1 (Level A) - 1.1.1';
    $lang['ImgEmptyAlt']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#non-text-content';
    $lang['ImgEmptyAlt']['level'] = 'alert';
    $lang['ImgEmptyAlt']['error_title'] = 'Non-decorative images without ALT text, limiting content available to screen reader user.';
    $lang['ImgEmptyAlt']['error_description'] = '';
    $lang['ImgEmptyAlt']['wga_description'] = '';

    $lang['RedundantAltText']['category'] = '1.1.1.H37.4';
    $lang['RedundantAltText']['wga_level'] = 'WCAG 2.1 (Level A) - 1.1.1';
    $lang['RedundantAltText']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#non-text-content';
    $lang['RedundantAltText']['level'] = 'alert';
    $lang['RedundantAltText']['error_title'] = 'Image ALT text duplicating image caption or body of a link, not helpful to screen reader user in understanding page content.';
    $lang['RedundantAltText']['error_description'] = '';
    $lang['RedundantAltText']['wga_description'] = '';

    $lang['EmptyAnchor']['category'] = '2.4.4.1';
    $lang['EmptyAnchor']['wga_level'] = 'WCAG 2.1 (Level A) - 2.4.4';
    $lang['EmptyAnchor']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#link-purpose-in-context';
    $lang['EmptyAnchor']['level'] = 'warning';
    $lang['EmptyAnchor']['error_title'] = 'Empty links or anchor tags.';
    $lang['EmptyAnchor']['error_description'] = '';
    $lang['EmptyAnchor']['wga_description'] = '';

    $lang['AbsoluteFontsize']['category'] = '1.4.4';
    $lang['AbsoluteFontsize']['wga_level'] = 'WCAG 2.1 (Level AA) - 1.4.4';
    $lang['AbsoluteFontsize']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#info-and-relationships';
    $lang['AbsoluteFontsize']['level'] = 'warning';
    $lang['AbsoluteFontsize']['error_title'] = 'Use of absolute font sizes, defined in pts and px.';
    $lang['AbsoluteFontsize']['error_description'] = '';
    $lang['AbsoluteFontsize']['wga_description'] = '';

    $lang['TextJustified']['category'] = '1.4.8.F88';
    $lang['TextJustified']['wga_level'] = 'WCAG 2.1 (Level AAA) - 1.4.8';
    $lang['TextJustified']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#visual-presentation';
    $lang['TextJustified']['level'] = 'warning';
    $lang['TextJustified']['error_title'] = 'Align or text-align used to justify text.';
    $lang['TextJustified']['error_description'] = '';
    $lang['TextJustified']['wga_description'] = '';

    $lang['MissingTd']['category'] = '1.3.1.H51';
    $lang['MissingTd']['wga_level'] = 'WCAG 2.1 (Level A) - 1.3.1';
    $lang['MissingTd']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#info-and-relationships';
    $lang['MissingTd']['level'] = 'warning';
    $lang['MissingTd']['error_title'] = 'Missing data cells in tables.';
    $lang['MissingTd']['error_description'] = '';
    $lang['MissingTd']['wga_description'] = '';

    $lang['NestedFieldset']['category'] = '1.3.1.H71';
    $lang['NestedFieldset']['wga_level'] = 'WCAG 2.1 (Level A) - 1.3.1';
    $lang['NestedFieldset']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#info-and-relationships';
    $lang['NestedFieldset']['level'] = 'warning';
    $lang['NestedFieldset']['error_title'] = 'Multiple fieldsets nested inside each other.';
    $lang['NestedFieldset']['error_description'] = '';
    $lang['NestedFieldset']['wga_description'] = '';

    $lang['MissingLangAttr']['category'] = '3.1.1.H57';
    $lang['MissingLangAttr']['wga_level'] = 'WCAG 2.1 (Level A) - 3.1.1';
    $lang['MissingLangAttr']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#language-of-page';
    $lang['MissingLangAttr']['level'] = 'normal';
    $lang['MissingLangAttr']['error_title'] = 'Missing language attribute on HTML elements.';
    $lang['MissingLangAttr']['error_description'] = '';
    $lang['MissingLangAttr']['wga_description'] = '';

    $lang['AmbiguousAnchor']['category'] = '2.4.4.2';
    $lang['AmbiguousAnchor']['wga_level'] = 'WCAG 2.1 (Level A) - 2.4.4';
    $lang['AmbiguousAnchor']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#link-purpose-in-context';
    $lang['AmbiguousAnchor']['level'] = 'warning';
    $lang['AmbiguousAnchor']['error_title'] = 'Generic links text, such as "more", "click here".';
    $lang['AmbiguousAnchor']['error_description'] = '';
    $lang['AmbiguousAnchor']['wga_description'] = '';

    $lang['ColorContrast']['category'] = '1.4.3.G18';
    $lang['ColorContrast']['wga_level'] = 'WCAG 2.1 (Level A) - 1.4.3';
    $lang['ColorContrast']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#contrast-minimum';
    $lang['ColorContrast']['level'] = 'warning';
    $lang['ColorContrast']['error_title'] = 'Low contrast ratio between foreground and background, less than 4.5:1';
    $lang['ColorContrast']['error_description'] = '';
    $lang['ColorContrast']['wga_description'] = '';

    $lang['ContextChangeForm']['category'] = '3.2.2.G13';
    $lang['ContextChangeForm']['wga_level'] = 'WCAG 2.1 (Level A) - 3.2.2';
    $lang['ContextChangeForm']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#on-input';
    $lang['ContextChangeForm']['level'] = 'warning';
    $lang['ContextChangeForm']['error_title'] = 'Missing notice in form fields that initiate context change.';
    $lang['ContextChangeForm']['error_description'] = '';
    $lang['ContextChangeForm']['wga_description'] = '';

    $lang['SkipNavLinks']['category'] = '2.4.1.G1';
    $lang['SkipNavLinks']['wga_level'] = 'WCAG 2.1 (Level A) - 2.4.1';
    $lang['SkipNavLinks']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#bypass-blocks';
    $lang['SkipNavLinks']['level'] = 'warning';
    $lang['SkipNavLinks']['error_title'] = 'Missing a link to skip navigation and to directly to page content.';
    $lang['SkipNavLinks']['error_description'] = '';
    $lang['SkipNavLinks']['wga_description'] = '';

    $lang['ObjectAltMissing']['category'] = '1.1.1.H53';
    $lang['ObjectAltMissing']['wga_level'] = 'WCAG 2.1 (Level A) - 1.1.1';
    $lang['ObjectAltMissing']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#non-text-content';
    $lang['ObjectAltMissing']['level'] = 'warning';
    $lang['ObjectAltMissing']['error_title'] = 'Missing ALT content for embedded content, not helpful to screen reader user in understanding page content.';
    $lang['ObjectAltMissing']['error_description'] = '';
    $lang['ObjectAltMissing']['wga_description'] = '';

    $lang['ImagemapMissingAlt']['category'] = '1.1.1.H24';
    $lang['ImagemapMissingAlt']['wga_level'] = 'WCAG 2.1 (Level A) - 1.1.1';
    $lang['ImagemapMissingAlt']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#non-text-content';
    $lang['ImagemapMissingAlt']['level'] = 'warning';
    $lang['ImagemapMissingAlt']['error_title'] = 'Missing ALT text for image map area tags.';
    $lang['ImagemapMissingAlt']['error_description'] = '';
    $lang['ImagemapMissingAlt']['wga_description'] = '';

    $lang['FieldsetWithoutLegend']['category'] = '1.3.1.F73';
    $lang['FieldsetWithoutLegend']['wga_level'] = 'WCAG 2.1 (Level A) - 1.3.1';
    $lang['FieldsetWithoutLegend']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#info-and-relationships';
    $lang['FieldsetWithoutLegend']['level'] = 'warning';
    $lang['FieldsetWithoutLegend']['error_title'] = 'Fieldsets do not providing a description using legend elements.';
    $lang['FieldsetWithoutLegend']['error_description'] = '';
    $lang['FieldsetWithoutLegend']['wga_description'] = '';

    $lang['EmptyHeadingTag']['category'] = '1.3.1.ARIA';
    $lang['EmptyHeadingTag']['wga_level'] = 'WCAG 2.1 (Level A) - 1.3.1';
    $lang['EmptyHeadingTag']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#info-and-relationships';
    $lang['EmptyHeadingTag']['level'] = 'warning';
    $lang['EmptyHeadingTag']['error_title'] = 'Heading tags empty.';
    $lang['EmptyHeadingTag']['error_description'] = '';
    $lang['EmptyHeadingTag']['wga_description'] = '';

    $lang['MissingThScope']['category'] = '1.3.1.H63';
    $lang['MissingThScope']['wga_level'] = 'WCAG 2.1 (Level A) - 1.3.1';
    $lang['MissingThScope']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#info-and-relationships';
    $lang['MissingThScope']['level'] = 'warning';
    $lang['MissingThScope']['error_title'] = 'Incorrect or missing scope attribute in table header cells.';
    $lang['MissingThScope']['error_description'] = '';
    $lang['MissingThScope']['wga_description'] = '';

    $lang['EmbedMissingAlt']['category'] = '1.1.1.H46';
    $lang['EmbedMissingAlt']['wga_level'] = 'WCAG 2.1 (Level A) - 1.1.1';
    $lang['EmbedMissingAlt']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#non-text-content';
    $lang['EmbedMissingAlt']['level'] = 'warning';
    $lang['EmbedMissingAlt']['error_title'] = 'Missing noembed tag with ALT content for an embed tag.';
    $lang['EmbedMissingAlt']['error_description'] = '';
    $lang['EmbedMissingAlt']['wga_description'] = '';

    $lang['MetaRefreshUse']['category'] = '2.2.1.F40';
    $lang['MetaRefreshUse']['wga_level'] = 'WCAG 2.1 (Level A) - 2.2.1';
    $lang['MetaRefreshUse']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#timing-adjustable';
    $lang['MetaRefreshUse']['level'] = 'warning';
    $lang['MetaRefreshUse']['error_title'] = 'Page refreshed after specific time by meta elements.';
    $lang['MetaRefreshUse']['error_description'] = '';
    $lang['MetaRefreshUse']['wga_description'] = '';

    $lang['IncorrectWhitespace']['category'] = '1.3.2.F32';
    $lang['IncorrectWhitespace']['wga_level'] = 'WCAG 2.1 (Level A) - 1.3.2';
    $lang['IncorrectWhitespace']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#meaningful-sequence';
    $lang['IncorrectWhitespace']['level'] = 'warning';
    $lang['IncorrectWhitespace']['error_title'] = 'Incorrect use of whitespace to create visual formatting.';
    $lang['IncorrectWhitespace']['error_description'] = '';
    $lang['IncorrectWhitespace']['wga_description'] = '';

    $lang['NestedTables']['category'] = '1.3.2.F49.2';
    $lang['NestedTables']['wga_level'] = 'WCAG 2.1 (Level A) - 1.3.2';
    $lang['NestedTables']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#meaningful-sequence';
    $lang['NestedTables']['level'] = 'warning';
    $lang['NestedTables']['error_title'] = 'Nested tables, not helpful to screen reader user in understanding page content.';
    $lang['NestedTables']['error_description'] = '';
    $lang['NestedTables']['wga_description'] = '';

    $lang['MissingAltTextAnchor']['category'] = '1.1.1.3';
    $lang['MissingAltTextAnchor']['wga_level'] = 'WCAG 2.1 (Level A) - 1.1.1';
    $lang['MissingAltTextAnchor']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#non-text-content';
    $lang['MissingAltTextAnchor']['level'] = 'warning';
    $lang['MissingAltTextAnchor']['error_title'] = 'Images without ALT text inside anchor tag.';
    $lang['MissingAltTextAnchor']['error_description'] = '';
    $lang['MissingAltTextAnchor']['wga_description'] = '';

    $lang['MissingTh']['category'] = '1.3.1.H51.2';
    $lang['MissingTh']['wga_level'] = 'WCAG 2.1 (Level A) - 1.3.1';
    $lang['MissingTh']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#info-and-relationships';
    $lang['MissingTh']['level'] = 'warning';
    $lang['MissingTh']['error_title'] = 'Missing Header cells in tables.';
    $lang['MissingTh']['error_description'] = '';
    $lang['MissingTh']['wga_description'] = '';

    $lang['ImgEmptyAltWithTitle']['category'] = '1.1.1.4';
    $lang['ImgEmptyAltWithTitle']['wga_level'] = 'WCAG 2.1 (Level A) - 1.1.1';
    $lang['ImgEmptyAltWithTitle']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#non-text-content';
    $lang['ImgEmptyAltWithTitle']['level'] = 'warning';
    $lang['ImgEmptyAltWithTitle']['error_title'] = 'Empty ALT text for images that have title and aria-label.';
    $lang['ImgEmptyAltWithTitle']['error_description'] = '';
    $lang['ImgEmptyAltWithTitle']['wga_description'] = '';

    $lang['ImgAltInvalidMedia']['category'] = '1.1.H37';
    $lang['ImgAltInvalidMedia']['wga_level'] = 'WCAG 2.1 (Level A) - 1.1';
    $lang['ImgAltInvalidMedia']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#non-text-content';
    $lang['ImgAltInvalidMedia']['level'] = 'warning';
    $lang['ImgAltInvalidMedia']['error_title'] = 'Non-descriptive ALT text that is not helpful to screen reader user in understanding page content.';
    $lang['ImgAltInvalidMedia']['error_description'] = '';
    $lang['ImgAltInvalidMedia']['wga_description'] = '';

    $lang['MissingThSpanScope']['category'] = '1.3.1.H63.2';
    $lang['MissingThSpanScope']['wga_level'] = 'WCAG 2.1 (Level A) - 1.3.1';
    $lang['MissingThSpanScope']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#info-and-relationships';
    $lang['MissingThSpanScope']['level'] = 'warning';
    $lang['MissingThSpanScope']['error_title'] = 'Rowgroup or colgroup not used on table header cells that span multiple rows or columns.';
    $lang['MissingThSpanScope']['error_description'] = '';
    $lang['MissingThSpanScope']['wga_description'] = '';

    $lang['MissingOnkeypress']['category'] = '2.1.1.F54';
    $lang['MissingOnkeypress']['wga_level'] = 'WCAG 2.1 (Level A) - 2.1.1';
    $lang['MissingOnkeypress']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#keyboard';
    $lang['MissingOnkeypress']['level'] = 'warning';
    $lang['MissingOnkeypress']['error_title'] = 'No equivalent keyboard event handler for mouse-based event handlers.';
    $lang['MissingOnkeypress']['error_description'] = '';
    $lang['MissingOnkeypress']['wga_description'] = '';

    $lang['NewWindowTag']['category'] = '3.2.1.G200';
    $lang['NewWindowTag']['wga_level'] = 'WCAG 2.1 (Level A) - 3.2.1';
    $lang['NewWindowTag']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#on-focus';
    $lang['NewWindowTag']['level'] = 'warning';
    $lang['NewWindowTag']['error_title'] = 'Missing notifiction on anchor tag that opens a new window.';
    $lang['NewWindowTag']['error_description'] = '';
    $lang['NewWindowTag']['wga_description'] = '';

    $lang['MissingTitle']['category'] = '2.4.2';
    $lang['MissingTitle']['wga_level'] = 'WCAG 2.1 (Level A) - 2.4.2';
    $lang['MissingTitle']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#page-titled';
    $lang['MissingTitle']['level'] = 'warning';
    $lang['MissingTitle']['error_title'] = 'Empty, invalid or missing page title.';
    $lang['MissingTitle']['error_description'] = '';
    $lang['MissingTitle']['wga_description'] = '';

    $lang['EmulatingLinks']['category'] = '2.1.1.F42';
    $lang['EmulatingLinks']['wga_level'] = 'WCAG 2.1 (Level A) - 2.1.1';
    $lang['EmulatingLinks']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#keyboard';
    $lang['EmulatingLinks']['level'] = 'warning';
    $lang['EmulatingLinks']['error_title'] = 'Failure of Success Criteria when emulating links.';
    $lang['EmulatingLinks']['error_description'] = '';
    $lang['EmulatingLinks']['wga_description'] = '';

    $lang['MissingThTd']['category'] = '1.3.1.H43';
    $lang['MissingThTd']['wga_level'] = 'WCAG 2.1 (Level A) - 1.3.1';
    $lang['MissingThTd']['wga_link'] = 'https://www.w3.org/TR/WCAG20-TECHS/F90.html';
    $lang['MissingThTd']['level'] = 'warning';
    $lang['MissingThTd']['error_title'] = 'Data cells do not using id and headers attributes to associate with header cells in data tables.';
    $lang['MissingThTd']['error_description'] = '';
    $lang['MissingThTd']['wga_description'] = '';

    $lang['ListIncorrectMarkup']['category'] = '1.3.1.H48';
    $lang['ListIncorrectMarkup']['wga_level'] = 'WCAG 2.1 (Level A) - 1.3.1';
    $lang['ListIncorrectMarkup']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#info-and-relationships';
    $lang['ListIncorrectMarkup']['level'] = 'warning';
    $lang['ListIncorrectMarkup']['error_title'] = 'Use of lists without HTML tags such as <ul>, <ol>. <li>';
    $lang['ListIncorrectMarkup']['error_description'] = '';
    $lang['ListIncorrectMarkup']['wga_description'] = '';

    $lang['DataTableMarkedPresentation']['category'] = '1.3.1.H51.3';
    $lang['DataTableMarkedPresentation']['wga_level'] = 'WCAG 2.1 (Level A) - 1.3.1';
    $lang['DataTableMarkedPresentation']['wga_link'] = 'http://www.w3.org/TR/WCAG20-TECHS/F92.html';
    $lang['DataTableMarkedPresentation']['level'] = 'warning';
    $lang['DataTableMarkedPresentation']['error_title'] = 'Data tables incorrectly marked with "presentation" role.';
    $lang['DataTableMarkedPresentation']['error_description'] = '';
    $lang['DataTableMarkedPresentation']['wga_description'] = '';

    $lang['IncorrectHeadingOrder']['category'] = '1.3.1.HEADINGS';
    $lang['IncorrectHeadingOrder']['wga_level'] = 'WCAG 2.1 (Level A) - 1.3.1';
    $lang['IncorrectHeadingOrder']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#info-and-relationships';
    $lang['IncorrectHeadingOrder']['level'] = 'warning';
    $lang['IncorrectHeadingOrder']['error_title'] = 'Page title not defined as H1; more than one H1 element on page; incorrect nesting of hedings on page (H3 before H2, H2 before H1).';
    $lang['IncorrectHeadingOrder']['error_description'] = '';
    $lang['IncorrectHeadingOrder']['wga_description'] = '';

    $lang['BlinkingText']['category'] = '2.2.2.G187';
    $lang['BlinkingText']['wga_level'] = 'WCAG 2.1 (Level A) - 2.2.2';
    $lang['BlinkingText']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#timing-adjustable';
    $lang['BlinkingText']['level'] = 'warning';
    $lang['BlinkingText']['error_title'] = 'Missing controls for stopping blinking or flashing animation.';
    $lang['BlinkingText']['error_description'] = '';
    $lang['BlinkingText']['wga_description'] = '';

    $lang['RedundantAnchorText']['category'] = '2.4.4.H33.2';
    $lang['RedundantAnchorText']['wga_level'] = 'WCAG 2.1 (Level A) - 2.4.4';
    $lang['RedundantAnchorText']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#link-purpose-in-context';
    $lang['RedundantAnchorText']['level'] = 'warning';
    $lang['RedundantAnchorText']['error_title'] = 'Several links were found with the same link text but with different values in the href attribute..';
    $lang['RedundantAnchorText']['error_description'] = '';
    $lang['RedundantAnchorText']['wga_description'] = '';

    $lang['ImgMissingAlt']['category'] = '1.1.1.5';
    $lang['ImgMissingAlt']['wga_level'] = 'WCAG 2.1 (Level A) - 1.1.1';
    $lang['ImgMissingAlt']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#non-text-content';
    $lang['ImgMissingAlt']['level'] = 'warning';
    $lang['ImgMissingAlt']['error_title'] = 'Missing ALT text for images.';
    $lang['ImgMissingAlt']['error_description'] = '';
    $lang['ImgMissingAlt']['wga_description'] = '';

    $lang['MissingFormLabel']['category'] = '3.3.2.G184';
    $lang['MissingFormLabel']['wga_level'] = 'WCAG 2.1 (Level A) - 3.3.2';
    $lang['MissingFormLabel']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#labels-or-instructions';
    $lang['MissingFormLabel']['level'] = 'warning';
    $lang['MissingFormLabel']['error_title'] = 'Missing labels in form fields.';
    $lang['MissingFormLabel']['error_description'] = '';
    $lang['MissingFormLabel']['wga_description'] = '';

    $lang['AvTagsMissingTrack']['category'] = '1.2.1';
    $lang['AvTagsMissingTrack']['wga_level'] = 'WCAG 2.1 (Level A) - 1.2.1';
    $lang['AvTagsMissingTrack']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#audio-only-and-video-only-prerecorded';
    $lang['AvTagsMissingTrack']['level'] = 'warning';
    $lang['AvTagsMissingTrack']['error_title'] = 'Missing text transcript for audio / video content.';
    $lang['AvTagsMissingTrack']['error_description'] = '';
    $lang['AvTagsMissingTrack']['wga_description'] = '';

    $lang['IframeMissingTitle']['category'] = '1.1.1.H64';
    $lang['IframeMissingTitle']['wga_level'] = 'WCAG 2.1 (Level A) - 1.1.1';
    $lang['IframeMissingTitle']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#non-text-content';
    $lang['IframeMissingTitle']['level'] = 'warning';
    $lang['IframeMissingTitle']['error_title'] = 'Missing title attributes for iframe tags.';
    $lang['IframeMissingTitle']['error_description'] = '';
    $lang['IframeMissingTitle']['wga_description'] = '';

    $lang['LinkWithoutVisualCue']['category'] = '1.4.1.F73';
    $lang['LinkWithoutVisualCue']['wga_level'] = 'WCAG 2.1 (Level A) - 1.4.1';
    $lang['LinkWithoutVisualCue']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#use-of-color';
    $lang['LinkWithoutVisualCue']['level'] = 'warning';
    $lang['LinkWithoutVisualCue']['error_title'] = 'Anchor text not clearly identified as such with underline or other styles.';
    $lang['LinkWithoutVisualCue']['error_description'] = '';
    $lang['LinkWithoutVisualCue']['wga_description'] = '';

    $lang['AvTagWithAutoplay']['category'] = '1.4.2';
    $lang['AvTagWithAutoplay']['wga_level'] = 'WCAG 2.1 (Level A) - 1.4.2';
    $lang['AvTagWithAutoplay']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#audio-control';
    $lang['AvTagWithAutoplay']['level'] = 'warning';
    $lang['AvTagWithAutoplay']['error_title'] = 'Autoplay without option to stop playback for audio or video elements.';
    $lang['AvTagWithAutoplay']['error_description'] = '';
    $lang['AvTagWithAutoplay']['wga_description'] = '';

    $lang['VisualFocusRemoved']['category'] = '2.4.7.F78';
    $lang['VisualFocusRemoved']['wga_level'] = 'WCAG 2.1 (Level AA) - 2.4.7';
    $lang['VisualFocusRemoved']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#focus-visible';
    $lang['VisualFocusRemoved']['level'] = 'warning';
    $lang['VisualFocusRemoved']['error_title'] = 'Visual indicator of focus removed with links or form field styles.';
    $lang['VisualFocusRemoved']['error_description'] = '';
    $lang['VisualFocusRemoved']['wga_description'] = '';

    $lang['ForegroundColorViolation']['category'] = '1.4.3.F24';
    $lang['ForegroundColorViolation']['wga_level'] = 'WCAG 2.1 (Level A) - 1.4.3';
    $lang['ForegroundColorViolation']['wga_link'] = 'https://www.w3.org/WAI/WCAG21/quickref/#contrast-minimum';
    $lang['ForegroundColorViolation']['level'] = 'warning';
    $lang['ForegroundColorViolation']['error_title'] = 'Only one color - either forreground or background - is specified,, without defining the other.';
    $lang['ForegroundColorViolation']['error_description'] = '';
    $lang['ForegroundColorViolation']['wga_description'] = '';

    return $lang;
  }
}
