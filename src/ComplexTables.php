<?php

/**
 * @file
 * Contains Drupal\ada_compliance\ComplexTables.
 */

namespace Drupal\ada_compliance;

/**
 * Class ComplexTables.
 *
 * @package Drupal\ada_compliance
 */

class ComplexTables {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $tables = $dom->getElementsByTagName('table');
    foreach ($tables as $table) {	
      $tablecode = $dom->saveXML($table, LIBXML_NOEMPTYTAG);
      if ((stristr($table->getAttribute('role'), "presentation") 
        || stristr($table->getAttribute('class'), "role-presentation"))
        && stristr($tablecode, 'rowspan')) {
        $result = $ErrorMessage::generateMessage($className, $tablecode, $num, $codes, $texts, $nid);
      }
    }
    return $result;
  }
}