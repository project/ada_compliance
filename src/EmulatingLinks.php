<?php

/**
 * @file
 * Contains Drupal\ada_compliance\EmulatingLinks.
 */

namespace Drupal\ada_compliance;

/**
 * Class EmulatingLinks.
 *
 * @package Drupal\ada_compliance
 */

class EmulatingLinks {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $elements = $dom->getElementsByTagName('*');
    $founderror = 0;
    foreach ($elements as $element) {
      if (($element->getAttribute('onclick') or $element->getAttribute('ondblclick') 
        or $element->getAttribute('onmousedown') or $element->getAttribute('onmouseup')
        or $element->getAttribute('onkeypress') or $element->getAttribute('onkeydown') 
        or $element->getAttribute('onkeyup') or $element->getAttribute('onmouseover') 
        or $element->getAttribute('onmouseout') or $element->getAttribute('onmousemove')
        or $element->getAttribute('onfocus') or $element->getAttribute('onblur')) 
        and $element->nodeName != 'a' 
        and $element->nodeName != 'button' 
        and $element->nodeName != 'input' 
        and $element->nodeName != 'select' 
        and $element->nodeName != 'textarea' 
        and $element->nodeName != 'area' 
        and $element->nodeName != 'datalist' 
        and $element->nodeName != 'output' 
        and (!$element->getAttribute('role')
        or !$element->hasAttribute('tabindex'))) {
          $ahtagcode = $dom->saveXML($element, LIBXML_NOEMPTYTAG);
          if (!$founderror) {
            $result = $ErrorMessage::generateMessage($className, $ahtagcode, $num, $codes, $texts, $nid);
          }
      }
    }
    return $result;
  }
}