<?php

namespace Drupal\ada_compliance\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

class DeleteForm extends ConfirmFormBase {

  protected $id;

  function getFormId() {
    return 'ada_compliance_delete';
  }

  function getQuestion() {
    return t('Are you sure you want to delete submission %id?', array('%id' => $this->id));
  }

  function getConfirmText() {
    return t('Delete');
  }

  function getCancelUrl() {
    return new Url('ada_compliance_list');
  }

  function buildForm(array $form, FormStateInterface $form_state) {
    $this->id = \Drupal::request()->get('id');
    return parent::buildForm($form, $form_state);
  }

  function submitForm(array &$form, FormStateInterface $form_state) {
    db_delete('ada_compliance_tests')->condition('id', $this->id)->execute();
    db_delete('ada_compliance_test_details')->condition('test_id', $this->id)->execute();
    db_delete('ada_compliance_test_links')->condition('test_id', $this->id)->execute();
    \Drupal::logger('ada_compliance')->notice('@type: deleted %title.',
        array(
            '@type' => 'ada compliance report',
            '%title' => $this->id,
        ));
    drupal_set_message(t('ADA compliance report %id has been deleted.', array('%id' => $this->id)));
    $form_state->setRedirect('ada_compliance_list');
  }

}