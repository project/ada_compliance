<?php

namespace Drupal\ada_compliance\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\SafeMarkup;
use Drupal\Core\Url;

use Drupal\ada_compliance\Texts;
use Drupal\ada_compliance\ErrorMessage;

class AddForm extends FormBase {

  protected $id;

  function getFormId() {
    return 'ada_compliance_add';
  }

  function getTestsHistory($arr) {
    $query = \Drupal::database()->select('ada_compliance_test_details', 'u');
    $query->condition('nid', $arr, 'IN');
    $query->fields('u', ['nid','is_global','is_errors','is_warnings','is_global_errors','is_global_warnings']);
    $results = $query->execute()->fetchAll();

    $output = array();
    foreach ($results as $result) {
      $output[$result->nid] = array($result->is_global, $result->is_errors, $result->is_warnings, $result->is_global_errors, $result->is_global_warnings);
    }
    return $output;
  }

  function buildForm(array $form, FormStateInterface $form_state) {

    $form['#tree'] = TRUE;
    $form['#attached']['library'][] = 'ada_compliance/ada-compliance-lib';

    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Launch tests'),
    );

    $form['detailsoptions'] = array(
      '#type' => 'label',
      '#title' => 'Web Content Accessibility Guidelines',
    );

    $texts = Texts::getTexts();
    $cnt = 0;
    for ($i=1; $i<=3; $i++) {

      $form['options'.$i] = array(
        '#type' => 'checkbox',
        '#title' => t('WCAG 2.1 Level '.str_repeat("A", $i).''),
        '#default_value' => true,
      );

    }

    $form['detailspages'] = array(
      '#type' => 'label',
      '#title' => t('Pages'),
    );

    $form['allcontenttypes'] = array(
      '#type' => 'checkbox',
      '#title' => t('Select / Deselect all content types'),
      '#default_value' => true,
    );

    $query = \Drupal::entityQuery('node')
      ->condition('status', 1) //published or not
      ->sort('title', 'ASC');
    $nids = $query->execute();

    $node_types = \Drupal\node\Entity\NodeType::loadMultiple();
    $node_type_options = [];
    foreach ($node_types as $node_type) {

      $content_pages = $arr = array();

      foreach ($nids as $nid) {   
         $node = \Drupal\node\Entity\Node::load($nid); 
         if ($node_type->id()!=$node->bundle()) continue;
         $content_pages[$nid] = $node->title->value;
         array_push($arr, $nid);
      }

      if (count($content_pages)) {

        $history_arr = self::getTestsHistory($arr);

        $normal = $alert = $warning = 0;
        foreach ($content_pages as $nid=>$title) {             
           if (count($history_arr) && isset($history_arr[$nid])) {
             if (!$history_arr[$nid][0] && !$history_arr[$nid][1] && !$history_arr[$nid][2] && !$history_arr[$nid][3] && !$history_arr[$nid][4])$normal++;
             if ($history_arr[$nid][1] || $history_arr[$nid][3])$alert++;
             if (!$history_arr[$nid][1] && !$history_arr[$nid][3] && ($history_arr[$nid][2] || $history_arr[$nid][4]))$warning++;
           }
        }

        $list = '';
        if ($normal) $list .= ($list ? ' ' : '').'<span class="ada_message_page ada_message_page_normal">Passed: '.$normal.'</span>';
        if ($alert) $list .= ($list ? ' ' : '').'<span class="ada_message_page ada_message_page_alert">Errors: '.$alert.'</span>';
        if ($warning) $list .= ($list ? ' ' : '').'<span class="ada_message_page ada_message_page_warning">Warnings: '.$warning.'</span>';
        if ($normal+$alert+$warning<count($content_pages)) $list .= ($list ? ' ' : '').'<span class="ada_message_page ada_message_page_notchecked">Not checked: '.(count($content_pages) - ($normal+$alert+$warning)).'</span>';
        if (!$list) $list .= ($list ? ' ' : '').'<span class="ada_message_page ada_message_page_notchecked">Not checked: '.count($content_pages).'</span>';

        $form['detailspages'.$node_type->id()] = array(
           '#type' => 'details',
           '#title' => '"'.$node_type->label().'" content type ('.count($content_pages).') '.t($list),
        );
    
        $form['detailspages'.$node_type->id()]['checkallpages'] = array(
           '#type' => 'checkbox',
           '#title' => t(''),
           '#default_value' => true,
           '#attributes' => array('class' => array('checkallpages')),
        );

        foreach ($content_pages as $nid=>$title) {  
           $class = '';
           if (count($history_arr) && isset($history_arr[$nid])) {
             if (!$history_arr[$nid][0] && !$history_arr[$nid][1] && !$history_arr[$nid][2] && !$history_arr[$nid][3] && !$history_arr[$nid][4])$class = 'ada_message_page_normal';
             if ($history_arr[$nid][1] || $history_arr[$nid][3])$class = 'ada_message_page_alert';
             if (!$history_arr[$nid][1] && !$history_arr[$nid][3] && ($history_arr[$nid][2] || $history_arr[$nid][4]))$class = 'ada_message_page_warning';
           }
           $form['detailspages'.$node_type->id()]['page'.$nid] = array(
             '#type' => 'checkbox',
             '#title' => t($title),
             '#default_value' => true,
             '#attributes' => array('class' => array('page-'.$node_type->id(),'page-checkbox', $class)),
           );    
        }
      }

    }

    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $query = \Drupal::entityQuery('node')
    ->condition('status', 1);
    $nids = $query->execute();

    $max_execution_time = ini_get('max_execution_time');
    $time_per_page = $this->finalSubmit($form, $form_state, 2);
    if (!$time_per_page) $time_per_page = $this->finalSubmit($form, $form_state, 2);
    $time_per_page_orig = $time_per_page;
    if (!$time_per_page || $time_per_page==1) $time_per_page = 2;
    $countOfCheckedPages = 0;
    $node_types = \Drupal\node\Entity\NodeType::loadMultiple();
    foreach ($nids as $nid) {
      $found = 0;
      foreach ($node_types as $node_type) {
          if ($form_state->getValue(['detailspages'.$node_type->id(),'page'.$nid])) {
              $found = 1;
              break;
          }
      }

      if (!$found) continue;

      $countOfCheckedPages++;
    }   
    $maxCountOfPages = floor($max_execution_time / ($time_per_page * 3));
    //echo count($nids)."=$max_execution_time=$time_per_page=$countOfCheckedPages=$maxCountOfPages";
    //die();
    if (!$form_state->getValue('options1') && !$form_state->getValue('options2') && !$form_state->getValue('options3')) {
      $form_state->setErrorByName('your_field', $this->t('Select please at least 1 option.'));
    } else if (!$countOfCheckedPages) {
      $form_state->setErrorByName('your_field', $this->t('Select please at least 1 page for test.'));
    } else if ($maxCountOfPages < $countOfCheckedPages) {
      $form_state->setErrorByName('your_field', $this->t('You have chosen to check '.$countOfCheckedPages.' pages, but only '.$maxCountOfPages.' pages can be checked at a time. Please reduce the number of pages to check.'));
    }
  }

  function submitForm(array &$form, FormStateInterface $form_state) {
        $this->finalSubmit($form, $form_state, 1);
  }

  /**
   * Handle submit.
   */
  private function finalSubmit(array &$form, FormStateInterface $form_state, $mode) {

    $url = trim($form_state->getValue('url'));
    if(strpos($url,'http://')===false && strpos($url,'https://')===false)$url = 'http://'.$url;
    $base_url = Url::fromRoute('<front>', [], ['absolute' => TRUE]);
    $base_url = $base_url->toString();

    $max_execution_time = ini_get('max_execution_time');
    $start_time = time();

    $aaa_error_arr = array();
    $aa_error_arr = array();
    $a_error_arr = array();

    $aaa_warning_arr = array();
    $aa_warning_arr = array();
    $a_warning_arr = array();

    $a_warning = $aa_warning = $aaa_warning = 0;
    $a_error = $aa_error = $aaa_error = 0;

    $texts = Texts::getTexts();
    $ErrorMessage = new ErrorMessage;
    $tests = $test_codes = array();
    foreach($texts as $key=>$data) {
      array_push($tests, $key);
      array_push($test_codes, $data['category']);
      if (strpos($data['wga_level'], '(Level AAA)')!==false && $data['level']=='warning')array_push($aaa_warning_arr, $data['category']);
      if (strpos($data['wga_level'], '(Level AAA)')!==false && $data['level']=='alert')array_push($aaa_error_arr, $data['category']);
      if (strpos($data['wga_level'], '(Level AA)')!==false && $data['level']=='warning')array_push($aa_warning_arr, $data['category']);
      if (strpos($data['wga_level'], '(Level AA)')!==false && $data['level']=='alert')array_push($aa_error_arr, $data['category']);
      if (strpos($data['wga_level'], '(Level A)')!==false && $data['level']=='warning')array_push($a_warning_arr, $data['category']);
      if (strpos($data['wga_level'], '(Level A)')!==false && $data['level']=='alert')array_push($a_error_arr, $data['category']);
    }

    $id = 0;
    if ($mode == 1) {
      $fields = array(array($form_state->getValue('options1'),$form_state->getValue('options2'),$form_state->getValue('options3')));
      $id = db_insert('ada_compliance_tests')->fields(array("settings"=>serialize($fields)))->execute();
    }

    $query = \Drupal::entityQuery('node')
    ->condition('status', 1);
    $nids = $query->execute();

    $total_errors = 0;
    $pages_with_errors = 0;

    $classes = array();

    for($i=0;$i<count($tests);$i++) {
      $useOption = 0;
      for($j=1;$j<=3;$j++) {
         if ($form_state->getValue('options'.$j) && strpos($texts[$tests[$i]]['wga_level'], '(Level '.str_repeat("A", $j).')')!==false)$useOption = 1;
      }
      if ($useOption) {
         $class = '\\Drupal\\ada_compliance\\'.$tests[$i];
         $classes[$test_codes[$i]] = new $class();
      }
    }

    $error_codes = array();

    $found_elements = array();
    $count_found_elements = array();

    $links_to_errors_array = array();

    $errors_arr = array();
    $errors_with_links_arr = array();

    $countOfCheckedPages = 0;

    $errors_list = '';

    $node_types = \Drupal\node\Entity\NodeType::loadMultiple();
    foreach ($nids as $nid) {
      $found = 0;
      foreach ($node_types as $node_type) {
          if ($form_state->getValue(['detailspages'.$node_type->id(),'page'.$nid])) {
              $found = 1;
              break;
          }
      }

      if (!$found) continue;

      if ($mode == 1) {
        if ((time() - $start_time + 10)>=$max_execution_time) { 
          // echo time().'='.$start_time.'='.(time() - $start_time + 10).'='.$max_execution_time;
          // die();
          break;
        }
      }

      $countOfCheckedPages++;
      if ($mode == 2 && $countOfCheckedPages > 1) break;

      $links_to_errors_array[$nid] = array();

      $node = \Drupal\node\Entity\Node::load($nid); 
      $title = $node->title->value;

      // Row with attributes on the row and some of its cells.
      $options = [
        'attributes' => [
          'target' => '_blank'
        ]
      ];

      $options = ['absolute' => TRUE];
      $url = \Drupal\Core\Url::fromRoute('entity.node.canonical', ['node' => $nid], $options);
      $url = $url->toString();

      $content = AddForm::curl_get_contents($url);

      $dom = new \DOMDocument;
      libxml_use_internal_errors(true);
      $dom->loadHTML($content);	

      $num = 0;
      $codes = array();
      $results = '';
      $links_to_errors = '<table class="error_list">';
      $top_errors = '';
      $site_url = \Drupal::request()->getHost();
      foreach($classes as $key=>$class) {
        $index = array_search($key, $test_codes);
        $count_errors_before = $num;
        $result = $class::check($dom, $num, $codes, str_replace("\n", "", $content), $texts, $ErrorMessage, $tests[$index], $site_url, $nid);
        $top_error = 0;
        if ($result && ($tests[$index]=="MissingHeadings" || $tests[$index]=="SkipNavLinks" || $tests[$index]=="IncorrectHeadingOrder")) {
           $top_errors .= "<code class=\"code_without_number ada_message_".$codes[count($codes)-1][3]."\"><span>".$codes[count($codes)-1][1]."</span><div style=\"display:none\">".$codes[count($codes)-1][2]."</div></code>";
           $top_error = 1;
        }

        if($result) {
          $errors_list.=($errors_list?',':'').$key;
          if (!isset($error_codes[$key])) $error_codes[$key] = 0;
          $error_codes[$key] += $num - $count_errors_before;
          $total_errors += $num - $count_errors_before;
          if (!$top_error) $results .= $result;
          $number = 0;
          if (!isset($links_to_errors_array[$nid][$key]) || !is_array($links_to_errors_array[$nid][$key])) $links_to_errors_array[$nid][$key] = array();
          array_push($links_to_errors_array[$nid][$key], array($count_errors_before, $num, array()));
        }
      }
      if($results)$pages_with_errors++;

      $links_to_errors .= '</table>';
      //echo time()."=".$start_time."=".(time()-$start_time);die();
      $content = $dom->saveXML();
      $content = str_replace("<br/>", "<br />", $content);
      $content = str_replace('"/>', '" />', $content);
      $content = str_replace('<tr/>','<tr></tr>', $content);
      $content = str_replace('></input>',' />', $content);
      $content = str_replace("><meta", ">\n<meta", $content);
      $content = str_replace("><link", ">\n<link", $content);
      $content = str_replace("><head", ">\n<head", $content);
      $content = str_replace('<?xml version="1.0" encoding="utf-8" standalone="yes"?>'."\n", '', $content);
      $content = str_replace('</head><body', "\n</head>\n<body", $content);
      $content = str_replace('><!--[if', ">\n<!--[if", $content);
      $content = str_replace('></body></html>', ">\n</body>\n</html>", $content);

      $content = str_replace("\n", "mynewline", $content);

      $messages = $message_description = $message_class = array();

      foreach($codes as $key=>$arr) {
        $c = $arr[0];

        if (!isset($found_elements[$arr[4]]) || !is_array($found_elements[$arr[4]])) {
          $found_elements[$arr[4]] = array();
          $count_found_elements[$arr[4]] = array();
        }
        if (!in_array($c, $found_elements[$arr[4]])) {
          array_push($found_elements[$arr[4]], $c);
          array_push($count_found_elements[$arr[4]], 1);
        } else {
          $index = array_search($c, $found_elements[$arr[4]]);
          $count_found_elements[$arr[4]][$index]++;
        }

        $c = str_replace('></area>',' />', $c);
        $c = str_replace('></img>',' />', $c);
        $c = str_replace('></input>',' />', $c);          
        $c = str_replace('></textarea>',' />', $c);          
        $c = str_replace('></meta>','>', $c);          
        $c = str_replace('<br></br>','<br />', $c);          
        $c = str_replace('></object>',' />', $c);
        $c = str_replace('<tr/>','<tr></tr>', $c);
        $c = str_replace("\n","mynewline",$c);

        $pos = strpos($content, $c);
        if($pos!==false) {
          $messages["mylabel$key"."end"] = $arr[1];
          $message_description["mylabel$key"."end"] = $arr[2];
          $message_class["mylabel$key"."end"] = $arr[3];
          $content = substr($content,0,$pos)."mylabel$key"."end".substr($content,$pos,strlen($content)-$pos);
        }
      }

      $page_tpl = $content;

      $content = str_replace("mynewline", "\n", $content);

      $result = '<pre class="code nid-'.$nid.'">'.$top_errors;
      $content_arr = explode("\n", $content);

      foreach($content_arr as $line) {
        $message_list = '';
        foreach($messages as $key=>$message) {
          if(strpos($line, $key)!==false) {
            $line = str_replace($key, "", $line);
            $message_list.="<code class=\"code_without_number ada_message_".$message_class[$key]."\"><span>$message</span><div style=\"display:none\">".$message_description[$key]."</div></code>";
          }
        }
        $line = str_replace('>','&gt;',str_replace('<','&lt;',$line));
        $result .= $message_list.'<code class="code_with_number">'.$line.'</code>';
      }
      $result .= '</pre>';

      $field_values = array("test_id"=>$id,"url"=>$url,"page_name"=>$title,"links_to_errors"=>"","details"=>$result,"nid"=>$nid,"errors_list"=>$errors_list,"page_tpl"=>$page_tpl,"placeholders_list"=>serialize(array($messages,$message_description,$message_class))); // $links_to_errors
      if ($mode == 1) db_insert('ada_compliance_test_details')->fields($field_values)->execute();

      if ($errors_list) { 
        $errors_list_arr = explode(',', $errors_list);  
        // $link = \Drupal::l($title, Url::fromUri($url, array('attributes'=>array('target'=>'_blank'))));
        foreach($errors_list_arr as $error) {
          $link = '<a class="use-ajax" href="'.$base_url.'/admin/content/ada_compliance/'.$id.'/'.$error.'/'.$nid.'">'.$title.'</a>';
          if (!isset($errors_arr[$error]) || !is_array($errors_arr[$error])) {
            $errors_arr[$error] = array();
            $errors_with_links_arr[$error] = "";
          }
          if (!in_array($nid, $errors_arr[$error])) {
            array_push($errors_arr[$error], $nid);
            $errors_with_links_arr[$error].= ($errors_with_links_arr[$error]?'<br>':'').$link;
          }
        }
      }

      $errors_list = '';
    }

    foreach($errors_with_links_arr as $error=>$links_list) {
      $field_values = array("test_id"=>$id,"error"=>$error,"links_list"=>$links_list);
      if ($mode == 1) db_insert('ada_compliance_test_links')->fields($field_values)->execute();
    }

    $count_of_global_errors = 0;
    $list_of_global_errors = '';
    $count_of_global_warnings = 0;
    $list_of_global_warnings = '';
    foreach($count_found_elements as $key=>$cnt_arr) {
      foreach($cnt_arr as $ind=>$cnt) {
        if ($cnt==$countOfCheckedPages) {
          $level = self::getLevelByCategory($texts, $key);
          foreach($links_to_errors_array as $nid=>$values) {
            array_push($links_to_errors_array[$nid][$key][0][2], $ind);
          }
          if($countOfCheckedPages>1) {
          if ($level=='warning') {
            $count_of_global_warnings = $count_of_global_warnings + $cnt;
            $list_of_global_warnings.=($list_of_global_warnings?', ':'').$key;
          } else {
            $count_of_global_errors = $count_of_global_errors + $cnt;
            $list_of_global_errors.=($list_of_global_errors?', ':'').$key;
          }
          }
          break;
        }
      }
    }

    $close_tag = '</td></tr>';

    foreach($links_to_errors_array as $nid=>$values) {

      $global_list = $alert_list = $warning_list = $global_error_list = $global_warning_list = '';

      foreach($values as $key=>$arr) {
        $links_to_errors_arr = array("","","","");
        $level = self::getLevelByCategory($texts, $key);
        $order = 2;
        if ($level=='warning') $order = 3;
        $open_tag = '<tr><td class="ada_message_'.$level.'">'.$key.'</td><td>';
        $number = $pass_numbers = 0;
        for ($i = $arr[0][0] + 1; $i <= $arr[0][1]; $i++) {
          $number++;
          $current_number = $number;	
          if (in_array($number-1,$arr[0][2])) {
            $pass_numbers++;
            $current_number = $pass_numbers;
            $order = 0;
            if ($level=='warning') $order = 1;
          }
          if ($current_number==8) {
            $links_to_errors_arr[$order].=' <a href="javascript:void(0)" class="open_rest_errors">...</a><div class="rest_of_errors">';
          }
          $links_to_errors_arr[$order].=' <a class="link_to_error" data-nid="'.$nid.'" href="#'.$nid.'_'.$i.'_'.$key.'">['.$current_number.']</a>';
          if ($i==$num && $current_number>=8) {
            $links_to_errors_arr[$order].='</div>';
          }
        }

        for ($i=0;$i<=3;$i++) {
          if (isset($links_to_errors_arr[$i]) && $links_to_errors_arr[$i]) {
            $value = $open_tag.$links_to_errors_arr[$i].$close_tag;
            if ($i==0 && $countOfCheckedPages==1)$i = 2;
            if ($i==1 && $countOfCheckedPages==1)$i = 3;
            switch($i) {
              case 0: $global_error_list .= $value;
                      $global_list .= $value;
                     break;
              case 1: $global_list .= $value;
                      $global_warning_list .= $value;
                     break;
              case 2: $alert_list .= $value;
                     break;
              case 3: $warning_list .= $value;
                     break;
            }
          }
        }
      }

      $links_to_errors = '';
      if ($global_list) $links_to_errors .= "<button class=\"accordion\">Multi-page issues</button><div class=\"panel\"><table class=\"error_list\">$global_list</table></div>";
      if ($alert_list) $links_to_errors .= "<button class=\"accordion\">Page-specific errors</button><div class=\"panel\"><table class=\"error_list\">$alert_list</table></div>";
      if ($warning_list) $links_to_errors .= "<button class=\"accordion\">Page-specific warnings</button><div class=\"panel\"><table class=\"error_list\">$warning_list</table></div>";

      if ($mode == 1) {
        db_update('ada_compliance_test_details')->fields(array(
          'links_to_errors' => $links_to_errors,
          'is_global' => ($global_list ? 1 : 0),
          'is_errors' => ($alert_list ? 1 : 0),
          'is_warnings' => ($warning_list ? 1 : 0),
          'is_global_errors' => ($global_error_list ? 1 : 0),
          'is_global_warnings' => ($global_warning_list ? 1 : 0)
        ))
        ->condition('test_id', $id)
        ->condition('nid', $nid)
        ->execute();
      }
    }

    $short_details_extra = '';
    foreach($error_codes as $code=>$count) {
      if ($count>0) {
        $short_details_extra .= ($short_details_extra?', ':'')."<a target=_blank href=\"./ada_compliance/reference#$code\">$code [$count]</a>"; // [$count]
        if (in_array($code, $aaa_warning_arr)) {
          $aaa_warning += $count;
        } else if (in_array($code, $aa_warning_arr)) {
          $aa_warning += $count;
        } else if (in_array($code, $a_warning_arr)) {
          $a_warning += $count;
        } else if (in_array($code, $aaa_error_arr)) {
          $aaa_error += $count;
        } else if (in_array($code, $aa_error_arr)) {
          $aa_error += $count;
        } else if (in_array($code, $a_error_arr)) {
          $a_error += $count;
        }
      }
    }

    $short_details = $countOfCheckedPages;
    $short_details2 = $pages_with_errors;
    $short_details3 = "errors: ".($aaa_error+$aa_error+$a_error)."<br>warnings: ".($aaa_warning+$aa_warning+$a_warning)."";
    $short_details4 = $short_details5 = $short_details6 = $short_details7 = '';
    if ($count_of_global_errors || $count_of_global_warnings) {
      $short_details4 = 'errors: '.$count_of_global_errors.'';
      $short_details4 .= '<br>warnings: '.$count_of_global_warnings.'';
    }

    if (!$form_state->getValue('options1')) {
      $short_details5 = "not tested";
    } else {
      $short_details5 = "errors: $a_error";
      $short_details5 .= "<br>warnings: $a_warning";
    }
    if (!$form_state->getValue('options2')) {
      $short_details6 = "not tested";
    } else {
      $short_details6 = "errors: $aa_error";
      $short_details6 .= "<br>warnings: $aa_warning";
    }
    if (!$form_state->getValue('options3')) {
      $short_details7 = "not tested";
    } else {
      $short_details7 = "errors: $aaa_error";
      $short_details7 .= "<br>warnings: $aaa_warning";
    }

    if ($mode == 1) {
      db_update('ada_compliance_tests')->fields(array(
        'short_details' => $short_details,
        'short_details2' => $short_details2,
        'short_details3' => $short_details3,
        'short_details4' => $short_details4,
        'short_details5' => $short_details5,
        'short_details6' => $short_details6,
        'short_details7' => $short_details7
        ))
      ->condition('id', $id)
      ->execute();

      $form_state->setRedirect('ada_compliance_list');
      return;
    }

    return time() - $start_time;

  }

  static function curl_get_contents($url) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSLVERSION, 5);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    if (curl_errno($ch)) {
      return false;
    } else {
      $data = curl_exec($ch);
      if (!$data) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        $data = curl_exec($ch);
      }
    }

    curl_close($ch);
    return $data;
  }

  static function getLevelByCategory($texts, $key) {
    foreach($texts as $data) {
      if ($data['category']==$key) return $data['level'];
    }
    return "alert";
  }

}