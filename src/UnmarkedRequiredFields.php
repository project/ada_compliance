<?php

/**
 * @file
 * Contains Drupal\ada_compliance\UnmarkedRequiredFields.
 */

namespace Drupal\ada_compliance;

/**
 * Class UnmarkedRequiredFields.
 *
 * @package Drupal\ada_compliance
 */

class UnmarkedRequiredFields {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $forms = $dom->getElementsByTagName('form');
    foreach ($forms as $form) {
      $errorcode = $dom->saveXML($form, LIBXML_NOEMPTYTAG);
      if (!stristr($errorcode, '*') && !stristr($errorcode, 'aria-required')) {
        if (self::count_form_fields($errorcode) > 1) {
          $result = $ErrorMessage::generateMessage($className, $errorcode, $num, $codes, $texts, $nid);
        }
      }
    }
    return $result;
  }

  static function count_form_fields($errorcode) {
    $count = 0;
    $minuscount = 0;
    $count += substr_count($errorcode, '<input');
    $count += substr_count($errorcode, '<select');
    $count += substr_count($errorcode, '<textarea');
	
    $minuscount += substr_count($errorcode, 'type="hidden"');
    $minuscount += substr_count($errorcode, "type='hidden'");
    $minuscount += substr_count($errorcode, "type='submit'");
    $minuscount += substr_count($errorcode, 'type="submit"');
    $count = $count - $minuscount;
	
    return $count;
  }

}