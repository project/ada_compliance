<?php

/**
 * @file
 * Contains Drupal\ada_compliance\ImgAltInvalid.
 */

namespace Drupal\ada_compliance;

/**
 * Class ImgAltInvalid.
 *
 * @package Drupal\ada_compliance
 */

class ImgAltInvalid {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $images = $dom->getElementsByTagName('img');
    $foundInvalidalt = 0;
    foreach ($images as $image) {
      if (isset($image)) {
        $error = 0;
        if (stristr($image->getAttribute('alt'), 'click this')) $error = 1;
        if (stristr($image->getAttribute('alt'), 'link to')) $error = 1;
        if (stristr($image->getAttribute('alt'), 'image of')) $error = 2;
        if (stristr($image->getAttribute('alt'), 'graphic of'))  $error = 2;
        if (stristr($image->getAttribute('alt'), 'photo of')) $error = 2;
        if (strtolower($image->getAttribute('alt')) == 'spacer') $error = 1;
        if (strtolower($image->getAttribute('alt')) == 'image') $error = 1;
        if (strtolower($image->getAttribute('alt')) == 'picture') $error = 1;
        for ($i=1; $i < 10; $i++) {
          if (strtolower($image->getAttribute('alt')) == 'picture '.$i) $error = 1;
          if (strtolower($image->getAttribute('alt')) == 'image '.$i) $error = 1;
          if (strtolower($image->getAttribute('alt')) == 'spacer '.$i) $error = 1;
          if (strtolower($image->getAttribute('alt')) == '000'.$i) $error = 1;
          if (strtolower($image->getAttribute('alt')) == 'intro#'.$i) $error = 1;
        }
        if (self::check_image_wrapped_in_anchor($dom, $image->getAttribute('alt'))) $error = 0;
        if ($error == 1) {
          $imagecode = $dom->saveXML($image, LIBXML_NOEMPTYTAG);
          if (!$foundInvalidalt) {
            $result = $ErrorMessage::generateMessage($className, $imagecode, $num, $codes, $texts, $nid);
          }
        }
      }
    }
    return $result;
  }

  static function check_image_wrapped_in_anchor($dom, $alt) {
    $links = $dom->getElementsByTagName('a');
    foreach ($links as $link) {
      $images = $link->getElementsByTagName('img');
      if (isset($images)) {
        foreach ($images as $image) {
          $src = $image->getAttribute('src');
          $href = $link->getAttribute('href');
          $extension = substr($image->getAttribute('src'),-4,4);
          if ($alt == $image->getAttribute('alt') and stristr($href, $extension)) return 1;
        }
      }
    }
    return 0;
  }
}