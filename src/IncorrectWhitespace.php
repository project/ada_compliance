<?php

/**
 * @file
 * Contains Drupal\ada_compliance\IncorrectWhitespace.
 */

namespace Drupal\ada_compliance;

/**
 * Class IncorrectWhitespace.
 *
 * @package Drupal\ada_compliance
 */

class IncorrectWhitespace {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $tags = array('p','h1','h2','h3','h4','h5','h6','pre','code');	
    $founderror = 0;
    foreach ($tags as $tag) {		
      $elements = $dom->getElementsByTagName($tag);	
      foreach ($elements as $element) {
        $patterns[] = '(\s\w\s\w\s\w\s)';
        $patterns[] = '\w\s{6,}\w';
        $html = htmlentities($ErrorMessage::GETinnerHTML($element));	
        if (preg_match('/(' .implode('|', $patterns) .')/i',$html)) {
          $errorcode = $dom->saveXML($element, LIBXML_NOEMPTYTAG);
          if (!$founderror) {
            $result = $ErrorMessage::generateMessage($className, $errorcode, $num, $codes, $texts, $nid);
          }
        }
      }
    }
    return $result;
  }
}