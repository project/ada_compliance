<?php

/**
 * @file
 * Contains Drupal\ada_compliance\MissingFormLabel.
 */

namespace Drupal\ada_compliance;

/**
 * Class MissingFormLabel.
 *
 * @package Drupal\ada_compliance
 */

class MissingFormLabel {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $forms = $dom->getElementsByTagName('form');
    $k = 0;
    foreach ($forms as $form) {
      $fields = $forms->item($k)->getElementsByTagName('input');
      $selects = $forms->item($k)->getElementsByTagName('select');
      $textareas = $forms->item($k)->getElementsByTagName('textarea');
      if (isset($labelfors)) unset($labelfors);
        $labels = $forms->item($k)->getElementsByTagName('label');
        foreach ($labels as $label) {
          if (isset($label)) $labelfors[] = $label->getAttribute('for');
        }
        if (!isset($labelfors)) $labelfors[] = "%^&*";
        $formAtttributestoIgnore[] = 'submit';
        $formAtttributestoIgnore[] = 'hidden';	
        foreach ($fields as $field) {
          $i = 0;   
          if (isset($field) and !in_array(strtolower($field->getAttribute('type')),$formAtttributestoIgnore)) {
            if (($field->getAttribute('id')== "" or !in_array($field->getAttribute('id'),$labelfors))
            and (($field->getAttribute('aria-labelledby') == "" 
            and $field->getAttribute('aria-label') == "" 
            and $field->getAttribute('title') == "" 
            and $field->getAttribute('type') != "image") 
            or ($field->getAttribute('type') == "image" 
            and $field->getAttribute('alt') == ""))) {
            $formfieldcode = $dom->saveXML($field, LIBXML_NOEMPTYTAG);
            if (!$foundField) {
              $result = $ErrorMessage::generateMessage($className, $formfieldcode, $num, $codes, $texts, $nid);
            }
          }
          $i++;
        }
      }
      foreach ($selects as $field) {
        $i = 0;   
        if (isset($field)  
        and ($field->getAttribute('id')== "" or !in_array($field->getAttribute('id'),$labelfors))
        and $field->getAttribute('aria-labelledby') == "" 
        and $field->getAttribute('aria-label') == "" 
        and $field->getAttribute('title') == "") {
          $formfieldcode = $dom->saveXML($field, LIBXML_NOEMPTYTAG);
          if (!$foundField) {
            $result = $ErrorMessage::generateMessage($className, $formfieldcode, $num, $codes, $texts, $nid);
          }
        }
        $i++;
      }	
      foreach ($textareas as $field) {
        $i = 0;   
        if (isset($field) 
        and ($field->getAttribute('id')== "" or !in_array($field->getAttribute('id'),$labelfors))
        and $field->getAttribute('aria-labelledby') == "" 
        and $field->getAttribute('aria-label') == "" 
        and $field->getAttribute('title') == "") {
          $formfieldcode = $dom->saveXML($field, LIBXML_NOEMPTYTAG);
          if (!$foundField) {
            $result = $ErrorMessage::generateMessage($className, $formfieldcode, $num, $codes, $texts, $nid);
          }
        }
        $i++;
      }	
      $k++;
    }
    return $result;
  }
}