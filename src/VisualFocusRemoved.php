<?php

/**
 * @file
 * Contains Drupal\ada_compliance\VisualFocusRemoved.
 */

namespace Drupal\ada_compliance;

/**
 * Class VisualFocusRemoved.
 *
 * @package Drupal\ada_compliance
 */

class VisualFocusRemoved {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $elements = $dom->getElementsByTagName('a');
    $visual_focus_removedfound = 0;
    foreach ($elements as $element) {
      if (
      preg_match("/outline:\s?((thin|\dpx)\s?|(dotted)\s?|(#000|black)\s?){3}/i", $element->getAttribute('style'))
      or preg_match('/border:\s?((medium|thick|\dpx)\s?|(solid)\s?|(#000|black)\s?){3}/',$element->getAttribute('style'))
      or (preg_match('/outline-width:\s?(thin|\dpx)/i',$element->getAttribute('style'))
      and preg_match('/outline-style:\s?(dotted)/i',$element->getAttribute('style'))
      and preg_match('/outline-color:\s?(#000|black)/i',$element->getAttribute('style')))
      or (preg_match('/border-width:\s?(medium|thick|\dpx)/i',$element->getAttribute('style'))
      and preg_match('/border-style:\s?(solid)/i',$element->getAttribute('style'))
      and preg_match('/border-color:\s?(#000|black)/i',$element->getAttribute('style')))) {
        $visual_focus_removed_errorcode = $dom->saveXML($element, LIBXML_NOEMPTYTAG);
        if (!$visual_focus_removedfound) {
          $result = $ErrorMessage::generateMessage($className, $visual_focus_removed_errorcode, $num, $codes, $texts, $nid);
        }
      }
    }
    return $result;
  }
}