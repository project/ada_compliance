<?php

/**
 * @file
 * Contains Drupal\ada_compliance\RedundantTitleTag.
 */

namespace Drupal\ada_compliance;

/**
 * Class RedundantTitleTag.
 *
 * @package Drupal\ada_compliance
 */

class RedundantTitleTag {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $links = $dom->getElementsByTagName('a');
    $foundredeidanttitletag = 0;
    foreach ($links as $link) {
      if ($link->getAttribute('title') != "") {
        if (isset($link) and strtolower($link->nodeValue) == strtolower($link->getAttribute('title'))) {
          $redeidanttitletag = $dom->saveXML($link, LIBXML_NOEMPTYTAG);
          if (!$foundredeidanttitletag) {
            $result = $ErrorMessage::generateMessage($className, $redeidanttitletag, $num, $codes, $texts, $nid);
          }
        }
      }
    }
    return $result;
  }
}