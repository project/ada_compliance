<?php

/**
 * @file
 * Contains Drupal\ada_compliance\ObjectAltMissing.
 */

namespace Drupal\ada_compliance;

/**
 * Class ObjectAltMissing.
 *
 * @package Drupal\ada_compliance
 */

class ObjectAltMissing {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $objecttags = $dom->getElementsByTagName('object');
    foreach ($objecttags as $object) {
      if (isset($objecttags) and $object->nodeValue == "") {
        $objectcode = $dom->saveXML($object, LIBXML_NOEMPTYTAG);
        if (!$foundmissingobjecttext) {
          $result = $ErrorMessage::generateMessage($className, $objectcode, $num, $codes, $texts, $nid);
        }
      }
    }
    return $result;
  }
}