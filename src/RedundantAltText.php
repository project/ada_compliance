<?php

/**
 * @file
 * Contains Drupal\ada_compliance\RedundantAltText.
 */

namespace Drupal\ada_compliance;

/**
 * Class RedundantAltText.
 *
 * @package Drupal\ada_compliance
 */

class RedundantAltText {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $images = $dom->getElementsByTagName('img');
    foreach ($images as $image) {
      if ($image->getAttribute('alt') != "") {
        if (isset($captioncode)) unset($captioncode);
        $pattern  = '/'."\[caption.*\](.*?)alt=[\"\']\b".preg_quote($image->getAttribute('alt'),'/')."\b[\"\'](.*?)\b".preg_quote($image->getAttribute('alt'),'/')."\b\[\/caption\]".'/';
        $redeidantalttag = $dom->saveXML($image, LIBXML_NOEMPTYTAG);
        if (preg_match($pattern,$content,$captioncode)) {
          if (!stristr($captioncode[0],'<a')) {
            if (!$foundredendantalt) {
              $result = $ErrorMessage::generateMessage($className, $redeidantalttag, $num, $codes, $texts, $nid);
            }
          }
        }
      }
    }
    $anchor = "";
    $figures = $dom->getElementsByTagName('figure');
    foreach ($figures as $figure) {
      $figurecaption = $figure->getElementsByTagName('figcaption')->item(0);
      $image = $figure->getElementsByTagName('img')->item(0);
      $anchor = $figure->getElementsByTagName('a')->item(0);
      if (isset($figurecaption)) {
        $figcaptioncode = $figurecaption->textContent;
        if ($anchor == "" and isset($image) and $figcaptioncode == $image->getAttribute('alt') and $image->getAttribute('alt') != "") {
          $caption_code = $dom->saveXML($figure, LIBXML_NOEMPTYTAG);
          if (!$foundredendantalt) {
            $result = $ErrorMessage::generateMessage($className, $figcaptioncode, $num, $codes, $texts, $nid);
          }
        }
      }
    }
    $figuredivs = $dom->getElementsByTagName('div');
    foreach ($figuredivs as $figure) {
      $figurecaption = $figure->getElementsByTagName('p')->item(0);
      $anchor = $figure->getElementsByTagName('a')->item(0);
      if ($anchor == "" and isset($figurecaption)) {
        $image = $figure->getElementsByTagName('img')->item(0);
        $figcaptioncode = $figurecaption->textContent;
        if (isset($image) 
        and $figcaptioncode == $image->getAttribute('alt') 
        and $image->getAttribute('alt') != "") {
          $caption_code = $dom->saveXML($figure, LIBXML_NOEMPTYTAG);
          if (!$foundredendantalt) {
            $result = $ErrorMessage::generateMessage($className, $figcaptioncode, $num, $codes, $texts, $nid);
          }
        }
      }	
    }
    $links = $dom->getElementsByTagName('a');
    foreach ($links as $link) {
      $images = $link->getElementsByTagName('img');
      foreach ($images as $image) {
        if ($image->getAttribute('alt') != "") {
          if (isset($link) and isset($image) and strtolower($link->nodeValue) == strtolower($image->getAttribute('alt'))) {
            $redeidantalttag = $dom->saveXML($link, LIBXML_NOEMPTYTAG);
            if (!$foundredendantalt) {
              $result = $ErrorMessage::generateMessage($className, $redeidantalttag, $num, $codes, $texts, $nid);
            }
          }
        }
      }
    }
    return $result;
  }
}