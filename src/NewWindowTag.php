<?php

/**
 * @file
 * Contains Drupal\ada_compliance\NewWindowTag.
 */

namespace Drupal\ada_compliance;

/**
 * Class NewWindowTag.
 *
 * @package Drupal\ada_compliance
 */

class NewWindowTag {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $links = $dom->getElementsByTagName('a');
    $foundnewwindowtag = 0;
    foreach ($links as $link) {
      $nodes = $link->childNodes;
      $imagealtiscompliant = 0;
      foreach ($nodes as $node) {
        if ($node->nodeName == "img") {
          if (stristr($node->getAttribute('title'),"new window") and stristr($node->getAttribute('aria-label'),"new window")) {
            $imagealtiscompliant = 1;
          }
        }
      }
      if (isset($link) 
      and (($link->getAttribute('target') == "_blank" 
      or stristr($link->getAttribute('onclick'),"window.open"))) 
      and !stristr($link->nodeValue,"new window") 
      and (!stristr($link->getAttribute('title'),"new window") and !stristr($link->getAttribute('aria-label'),"new window"))
      and !$imagealtiscompliant) {
        $newwindowtag = $dom->saveXML($link, LIBXML_NOEMPTYTAG);
        if (!$foundnewwindowtag) {
          $result = $ErrorMessage::generateMessage($className, $newwindowtag, $num, $codes, $texts, $nid);
        }
      }
    }
    return $result;
  }
}