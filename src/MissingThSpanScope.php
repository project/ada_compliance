<?php

/**
 * @file
 * Contains Drupal\ada_compliance\MissingThSpanScope.
 */

namespace Drupal\ada_compliance;

/**
 * Class MissingThSpanScope.
 *
 * @package Drupal\ada_compliance
 */

class MissingThSpanScope {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $tables = $dom->getElementsByTagName('table');
    $k = 0;
    $foundScope = 0;
    foreach ($tables as $table) {
      $tablecode = $dom->saveXML($table, LIBXML_NOEMPTYTAG);
      $headercells = $tables->item($k)->getElementsByTagName('th');
      foreach ($headercells as $th) {
        if (isset($th) and (($th->getAttribute('colspan') > 1 and $th->getAttribute('scope') != "colgroup" and $th->getAttribute('scope') != "row" and $th->getAttribute('id') == "") or ($th->getAttribute('rowspan') > 1 and $th->getAttribute('scope') != "rowgroup" and $th->getAttribute('id') == ""))) {
          if (!$foundScope) {
            $result = $ErrorMessage::generateMessage($className, $tablecode, $num, $codes, $texts, $nid);
          }
        }
      }
      $k++;
    }
    return $result;
  }
}