<?php

/**
 * @file
 * Contains Drupal\ada_compliance\DataTableMarkedPresentation.
 */

namespace Drupal\ada_compliance;

/**
 * Class DataTableMarkedPresentation.
 *
 * @package Drupal\ada_compliance
 */

class DataTableMarkedPresentation {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $tables = $dom->getElementsByTagName('table');
    $foundMisMarkedTable = 0;
    foreach ($tables as $table) {	
      $tablecode = $dom->saveXML($table, LIBXML_NOEMPTYTAG);
      if ($table != "" 
        and stristr($table->getAttribute('role'), "presentation") 
        and (stristr(self::stripInnerTables($tablecode), "<th") 
        or $table->getAttribute('summary') != "" 
        or stristr(self::stripInnerTables($tablecode), "<caption>"))) {
          if (!$foundMisMarkedTable) {
            $result = $ErrorMessage::generateMessage($className, $tablecode, $num, $codes, $texts, $nid);
          }
      }
    }
    return $result;
  }

  function stripInnerTables($html) {
    $dom = new \DOMDocument;
    $dom->preserveWhiteSpace = false;
    $dom->loadHTML($html);
    $remainImages = array(0);
    $nodes = $dom->getElementsByTagName("table");
    for ($i = 0; $i < $nodes->length; $i++) {
      if (!in_array($i,$remainImages)) {
        $image = $nodes->item($i);
        $image->parentNode->removeChild($image);
      }  
    }
    $newhtml = $dom->saveHTML();
    return $newhtml;	
  }	
}