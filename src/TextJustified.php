<?php

/**
 * @file
 * Contains Drupal\ada_compliance\TextJustified.
 */

namespace Drupal\ada_compliance;

/**
 * Class TextJustified.
 *
 * @package Drupal\ada_compliance
 */

class TextJustified {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = '';
    $fontsearchpatterns[] = "|(text-)?align:\s?justify|i";
    $text_justifiedfound = 0;
    foreach($fontsearchpatterns as $key => $pattern) {
      if (preg_match_all($pattern, $content, $matches, PREG_OFFSET_CAPTURE)) {
        $matchsize = sizeof($matches[0]);
        for ($i=0; $i < $matchsize; $i++) {
          if (isset($matches[0][$i][0]) and $matches[0][$i][0] != "") {
            $text_justified_errorcode = htmlspecialchars($matches[0][$i][0]);
            if (!$text_justifiedfound) {
              $result = $ErrorMessage::generateMessage($className, $text_justified_errorcode, $num, $codes, $texts, $nid);
            }
          }
        }
      }
    }
    return $result;
  }
}