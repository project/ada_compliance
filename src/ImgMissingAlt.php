<?php

/**
 * @file
 * Contains Drupal\ada_compliance\ImgMissingAlt.
 */

namespace Drupal\ada_compliance;

/**
 * Class ImgMissingAlt.
 *
 * @package Drupal\ada_compliance
 */

class ImgMissingAlt {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $tags = array('img', 'input');	
    $foundImage = 0;
    foreach ($tags as $tag) {		
      $elements = $dom->getElementsByTagName($tag);	
      foreach ($elements as $element) {	
        if (isset($element) and ($element->nodeName == 'img' 
          and !$element->hasAttribute('alt') and $element->getAttribute('role') != "presentation")
          or($element->nodeName == 'input' 
          and !$element->hasAttribute('alt') and $element->getAttribute('type') == "image")) {
          $imagecode = $dom->saveXML($element, LIBXML_NOEMPTYTAG);
          if (!$foundImage) {
            $result = $ErrorMessage::generateMessage($className, $imagecode, $num, $codes, $texts, $nid);
          }
        }
      }
    }
    return $result;
  }
}