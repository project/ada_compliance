<?php

/**
 * @file
 * Contains Drupal\ada_compliance\IncorrectHeadingOrder.
 */

namespace Drupal\ada_compliance;

/**
 * Class IncorrectHeadingOrder.
 *
 * @package Drupal\ada_compliance
 */

class IncorrectHeadingOrder {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $errorcode = '';
    $foundIssue = 0;
    $h1 = $dom->getElementsByTagName('h1')->length;	
    $h2 = $dom->getElementsByTagName('h2')->length;
    $h3 = $dom->getElementsByTagName('h3')->length;
    $h4 = $dom->getElementsByTagName('h4')->length;
    $h5 = $dom->getElementsByTagName('h5')->length;
    $h6 = $dom->getElementsByTagName('h6')->length;	
    if ($h2 and !$h1) {
      $errorcode .= 'h2 without h1; ';
      $h2s = $dom->getElementsByTagName('h2');
      foreach ($h2s as $h2value) {
        $errorcode .= $dom->saveXML($h2value, LIBXML_NOEMPTYTAG);
      }	
    }
    if ($h3 and !$h2) {
      $errorcode .= 'h3 without h2; ';
      $h3s = $dom->getElementsByTagName('h3');
      foreach ($h3s as $h3value){
        $errorcode .= $dom->saveXML($h3value, LIBXML_NOEMPTYTAG);
      }	
    }
    if ($h4 and !$h3) {
      $errorcode .= 'h4 without h3; ';
      $h4s = $dom->getElementsByTagName('h4');
      foreach ($h4s as $h4value) {
        $errorcode .= $dom->saveXML($h4value, LIBXML_NOEMPTYTAG);
      }	
    }
    if ($h5 and !$h4) {
      $errorcode .= 'h5 without h4; ';
      $h5s = $dom->getElementsByTagName('h5');
      foreach ($h5s as $h5value) {
        $errorcode .= $dom->saveXML($h5value, LIBXML_NOEMPTYTAG);
      }		
    }
    if ($h6 and !$h5) {
      $errorcode .= 'h6 without h5; ';
      $h6s = $dom->getElementsByTagName('h6');
      foreach ($h6s as $h6value) {
        $errorcode .= $dom->saveXML($h6value, LIBXML_NOEMPTYTAG);
      }		
    }
    if ($errorcode != '') {
      $errorcode = 'Issues: '.$errorcode;	
      if (!$foundIssue) {
        $result = $ErrorMessage::generateMessage($className, $errorcode, $num, $codes, $texts, $nid);
      }
    }
    return $result;
  }
}