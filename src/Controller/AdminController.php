<?php
/**
@file
Contains \Drupal\ada_compliance\Controller\AdminController.
 */

namespace Drupal\ada_compliance\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AlertCommand;
use Drupal\Core\Ajax\InvokeCommand;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\ada_compliance\Texts;

class AdminController extends ControllerBase {

  protected $id;

  public function getHTML($id, $error, $nid) {

    # New response
    //$response = new AjaxResponse();

    # Commands Ajax
    //$response->addCommand(new AlertCommand('Hello ' .$nid.'='.$error));

    # Return response
    //return $response;

    //$markup = '<h1>H3llo!</h1>';
    //return ['#markup' => $markup];

    $query = \Drupal::database()->select('ada_compliance_test_details', 'u');
    $query->condition('test_id', $id);
    $query->condition('nid', $nid);
    $query->fields('u', ['page_tpl','placeholders_list']);
    $results = $query->execute()->fetchAll();
    $page_tpl = $results[0]->page_tpl;
    $placeholders_list = unserialize($results[0]->placeholders_list);

    $messages = $placeholders_list[0];
    $message_description = $placeholders_list[1];
    $message_class = $placeholders_list[2];

      $content = str_replace("mynewline", "\n", $page_tpl);

      $result = '<pre class="code nid-'.$nid.'" style="display:block">';
      $content_arr = explode("\n", $content);

      foreach($content_arr as $line) {
        $message_list = '';
        foreach($messages as $key=>$message) {
          if(strpos($line, $key)!==false) {
            $line = str_replace($key, "", $line);
            if (strpos($message, '<strong>'.$error.'.</strong>')!==false) {
              $message_list.="<code class=\"code_without_number ada_message_".$message_class[$key]."\"><span>$message</span><div style=\"display:none\">".$message_description[$key]."</div></code>";
            }
          }
        }
        $line = str_replace('>','&gt;',str_replace('<','&lt;',$line));
        $result .= $message_list.'<code class="code_with_number">'.$line.'</code>';
      }
      $result .= '</pre>';


    $response = new AjaxResponse();
    $response->addCommand(new InvokeCommand(NULL, 'myAjaxCallback', [$error, $result]));
    return $response;

  }

  function getReportDetails($id) {
    $query = \Drupal::database()->select('ada_compliance_tests', 'u');
    $query->condition('id', $this->id);
    $query->fields('u', ['test_date', 'short_details', 'short_details2', 'short_details3', 'short_details4', 'short_details5', 'short_details6', 'short_details7']);
    $results = $query->execute()->fetchAll(); // Assoc('id')

    $history = '';
    foreach ($results as $nid) {
      $test_date = $nid->test_date;
      $short_details = (isset($nid->short_details)?$nid->short_details:'');
      $short_details2 = (isset($nid->short_details2)?$nid->short_details2:'');
      $short_details3 = (isset($nid->short_details3)?$nid->short_details3:'');
      $short_details4 = (isset($nid->short_details4)?$nid->short_details4:'');
      $short_details5 = (isset($nid->short_details5)?$nid->short_details5:'');
      $short_details6 = (isset($nid->short_details6)?$nid->short_details6:'');
      $short_details7 = (isset($nid->short_details7)?$nid->short_details7:'');
      $history = 'Report created: '.date("m/d/Y - H:i", strtotime($test_date)).'<br>';
      $history .= 'Number of pages checked: '.$short_details.'<br>';
      $history .= 'Number of pages with issues: '.$short_details2.'<br>';
      $history .= '<table class="ada-table responsive-enabled">';
      $history .= '<thead><tr><th>WCAG 2.1 total</th><th>Multi-page issues</th><th>WCAG 2.1 Level A</th><th>WCAG 2.1 Level AA</th><th>WCAG 2.1 Level AAA</th></tr></thead><tbody><tr><td>'.$short_details3.'</td>';
      $history .= '<td>'.$short_details4.'</td>';
      $history .= '<td>'.$short_details5.'</td>';
      $history .= '<td>'.$short_details6.'</td>';
      $history .= '<td>'.$short_details7.'</td></tr></tbody></table>';
      break;
    }
    return $history;
  }

  function getErrorsCount($id, $field, $value) {
      $query = \Drupal::database()->select('ada_compliance_test_details', 'u');
      $query->condition('test_id', $id);
      $query->condition($field, $value);
      $query->addExpression('COUNT(*)');
      return $query->execute()->fetchField();
  }

  function check() {

    $this->id = \Drupal::request()->get('id');

    $filter = \Drupal::request()->get('filter');
    if ($filter) {
      $multipage = \Drupal::request()->get('multipage');
      $errors = \Drupal::request()->get('errors');
      $warnings = \Drupal::request()->get('warnings');
    } else {
      $multipage = $this->getErrorsCount($this->id, 'is_global', 1);
      if ($multipage > 0) $multipage = 1;
      $errors = $this->getErrorsCount($this->id, 'is_errors', 1);
      if ($errors > 0) $errors = 1;
      $warnings = $this->getErrorsCount($this->id, 'is_warnings', 1);
      if ($warnings > 0) $warnings = 1;
    }
    if (!$multipage) $multipage = 0;
    if (!$errors) $errors = 0;
    if (!$warnings) $warnings = 0;

    $url = Url::fromRoute('ada_compliance_list');

    $url = \Drupal::l(t('Launch ADA compliance check'), $url);
    $url = str_replace("ada_compliance", "ada_compliance/check", $url);

    $url2 = $url;
    $url2 = str_replace("ada_compliance/check", "ada_compliance/reference", $url2);
    $url2 = str_replace("Launch ADA compliance check", "View category reference", $url2);
    $url2 = str_replace("a href", "a target=_blank href", $url2);

    $report_details = $this->getReportDetails($this->id);

    $add_link = '<p>' . $url2 . '</p><div>'.$report_details.'</div>';

    $form['markup'] = [
      '#type' => 'markup',
      '#markup' => $add_link,
      '#attached' => array('library' => array('ada_compliance/ada-compliance-lib'))
    ];

    $form['filters'] = array(
       '#type' => 'table',
       '#attributes' => [
         'class' => ['ada-filter-table'],
       ]
    );

    $form['filters'][0]['multipageissues'] = array(
      '#type' => 'checkbox',
      '#name' => 'multipageissues',
      '#title' => t('Multi-page issues'),
      '#checked' => ($multipage > 0 ? true : false),
    );

    $form['filters'][0]['pagespecificerrors'] = array(
      '#type' => 'checkbox',
      '#name' => 'pagespecificerrors',
      '#title' => t('Page-specific errors'),
      '#checked' => ($errors > 0 ? true : false),
    );

    $form['filters'][0]['pagespecificwarnings'] = array(
      '#type' => 'checkbox',
      '#name' => 'pagespecificwarnings',
      '#title' => t('Page-specific warnings'),
      '#checked' => ($warnings > 0 ? true : false),
    );

    $form['filters'][0]['actions'] = array('#type' => 'actions');
    $form['filters'][0]['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Filter'),
    );

/*
    $form['multipageissues'] = array(
      '#type' => 'checkbox',
      '#name' => 'multipageissues',
      '#title' => t('Multi-page issues'),
      '#checked' => ($multipage > 0 ? true : false),
    );

    $form['pagespecificerrors'] = array(
      '#type' => 'checkbox',
      '#name' => 'pagespecificerrors',
      '#title' => t('Page-specific errors'),
      '#checked' => ($errors > 0 ? true : false),
    );

    $form['pagespecificwarnings'] = array(
      '#type' => 'checkbox',
      '#name' => 'pagespecificwarnings',
      '#title' => t('Page-specific warnings'),
      '#checked' => ($warnings > 0 ? true : false),
    );

    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Filter'),
    );
*/

    // Table header.
    $header = array(
      'page_name' => t('Page'),
      'url' => t('HTML Code'),
    );

    //$condition_or = new \Drupal\Core\Database\Query\Condition('OR');
    //if ($multipage) $condition_or->condition('u.is_global', $multipage);
    //if ($errors) $condition_or->condition('u.is_errors', $errors);
    //if ($warnings) $condition_or->condition('u.is_warnings', $warnings);

    $query = \Drupal::database()->select('ada_compliance_test_details', 'u');

    $condition_or = $query->orConditionGroup();
    if ($multipage) $condition_or->condition('u.is_global', $multipage);
    if ($errors) $condition_or->condition('u.is_errors', $errors);
    if ($warnings) $condition_or->condition('u.is_warnings', $warnings);

    $query->condition('test_id', $this->id);
    if ($multipage || $errors || $warnings) $query->condition($condition_or);
    if (!$multipage && !$errors && !$warnings) {
      $query->condition('is_global', 0);
      $query->condition('is_errors', 0);
      $query->condition('is_warnings', 0);
    }
    $query->fields('u', ['id','url','page_name','details','links_to_errors','nid']);
    $query->orderBy('is_global', 'DESC');
    $query->orderBy('is_errors', 'DESC');
    $query->orderBy('is_warnings', 'DESC');
    $query->orderBy('page_name', 'ASC');
    //For the pagination we need to extend the pagerselectextender and
    //limit in the query
    $pager = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit(10);
    $results = $pager->execute()->fetchAll();

    $output = array();
    foreach ($results as $nid) {
      $url = $nid->url;
      $title = $nid->page_name;
      $result = $nid->details;
      $links_to_errors = $nid->links_to_errors;
      $output[$nid->id] = [
        'data' => array(
          t('<div class="stickyDa">'.\Drupal::l($title, Url::fromUri($url, array('attributes'=>array('target'=>'_blank')))).'<br>'.'<p><a href="javascript:void(0)" name="report-'.$nid->nid.'" class="show-report">[ Show issues in code ]</a></p>'.$links_to_errors.'</div>'),
          t($result)
        )
      ];
    }

    $form['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $output,
      '#empty' => t('No reports found'),
      '#attributes' => [
        'class' => ['ada-table'],
      ],
    ];

    $form['pager'] = array(
      '#type' => 'pager'
    );

    return $form;

  }

  function checkerror() {

    $this->id = \Drupal::request()->get('id');

    $url = Url::fromRoute('ada_compliance_list');

    $url = \Drupal::l(t('Launch ADA compliance check'), $url);
    $url = str_replace("ada_compliance", "ada_compliance/checkerror", $url);

    $url2 = $url;
    $url2 = str_replace("ada_compliance/checkerror", "ada_compliance/reference", $url2);
    $url2 = str_replace("Launch ADA compliance check", "View category reference", $url2);
    $url2 = str_replace("a href", "a target=_blank href", $url2);

    $report_details = $this->getReportDetails($this->id);

    $add_link = '<p>' . $url2 . '</p><div>'.$report_details.'</div>';

    $text = array(
      '#type' => 'markup',
      '#markup' => $add_link,
      '#attached' => array('library' => array('ada_compliance/ada-compliance-lib'))
    );

    $form['markup'] = [
      '#type' => 'markup',
      '#markup' => $add_link,
      '#attached' => array('library' => array('ada_compliance/ada-compliance-lib'))
    ];

    $header = array(
      'page_name' => t('Errors'),
      'html_code' => t('HTML Code'),
    );

    $output = array();
    $query = \Drupal::database()->select('ada_compliance_test_links', 'u');
    $query->condition('test_id', $this->id);
    $query->fields('u', ['id','error','links_list']);
    $query->orderBy('error', 'ASC');
    //For the pagination we need to extend the pagerselectextender and
    //limit in the query
    $pager = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit(10);
    $results = $pager->execute()->fetchAll();

    $output = array();
    foreach ($results as $nid) {
      $error = $nid->error;
      $links_list = $nid->links_list;
      $output[$nid->id] = [
        'data' => array(
          t($error.'<br>'.$links_list),
          t('<div id="error_'.$error.'"></div>')
        )
      ];
    }

    $form['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $output,
      '#empty' => t('No reports found'),
      '#attributes' => [
        'class' => ['ada-table'],
      ],
    ];

    $form['pager'] = array(
      '#type' => 'pager'
    );

    return $form;
  }


  function reference() {

    $url = Url::fromRoute('ada_compliance_list');

    $text = array(
      '#type' => 'markup',
      '#attached' => array('library' => array('ada_compliance/ada-compliance-lib'))
    );

    $header = array(
      'code' => t('Code'),
      'summary' => t('Summary'),
      // 'details' => t('Details'),
    );

    $texts = Texts::getTexts();

    $reference_arr = array();

    foreach($texts as $class=>$data) {
	// array_push($reference_arr, array($data['category'], $data['wga_level'].' '.$data['error_title'], ($data['wga_level']?'<a href="'.$data['wga_link'].'" target="_blank" class="adaNewWindowInfo">'.$data['wga_level'].'</a>':''))); // $data['wga_description'].
	array_push($reference_arr, array($data['category'], '<a href="'.$data['wga_link'].'" target="_blank" class="adaNewWindowInfo">'.$data['wga_level'].'</a>'.' '.$data['error_title'] )); // $data['wga_description'].
    }

    $rows = array();
    foreach ($reference_arr as $ref) {
      $rows[] = array(
        'data' => array(
          t("<a name=\"".$ref[0]."\" id=\"".$ref[0]."\"></a>".$ref[0]),
          t($ref[1]),
          // t($ref[2])
        )
      );
    }
    $table = array(
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows
    );

    return array(
      $text,
      $table,
    );
  }

  function content() {

    $url = Url::fromRoute('ada_compliance_add');
    $nids = db_query('SELECT * FROM {ada_compliance_tests} ORDER BY test_date DESC')->fetchAllAssoc('id');
    $url = \Drupal::l(t('Launch ADA compliance test'), $url);

    $add_link = '<p>' . $url . '</p>';
    $text = array(
      '#type' => 'markup',
      '#markup' => $add_link,
      '#attached' => array('library' => array('ada_compliance/ada-compliance-lib'))
    );

    $header = array(
      'test_date' => t('Created'),
      'short_details' => t('Number of pages checked'),
      'short_details2' => t('Number of pages with issues'),
      'short_details3' => t('WCAG 2.1 total'),
      'short_details4' => t('Multi-page issues'),
      'short_details5' => t('WCAG 2.1 Level A'),
      'short_details6' => t('WCAG 2.1 Level AA'),
      'short_details7' => t('WCAG 2.1 Level AAA'),
      'view' => t('View pages'),
      'viewerror' => t('View errors'),
      'delete' => t('Delete'),
    );

    $rows = array();
    foreach ($nids as $id=>$nid) {
      $test_date = $nid->test_date;
      $short_details = (isset($nid->short_details)?$nid->short_details:'');
      $short_details2 = (isset($nid->short_details2)?$nid->short_details2:'');
      $short_details3 = (isset($nid->short_details3)?$nid->short_details3:'');
      $short_details4 = (isset($nid->short_details4)?$nid->short_details4:'');
      $short_details5 = (isset($nid->short_details5)?$nid->short_details5:'');
      $short_details6 = (isset($nid->short_details6)?$nid->short_details6:'');
      $short_details7 = (isset($nid->short_details7)?$nid->short_details7:'');
      $options = [
        'attributes' => [
          'target' => '_blank'
        ]
      ];
      $viewUrl = Url::fromRoute('ada_compliance_view', array('id' => $id), $options);
      $viewUrl2 = Url::fromRoute('ada_compliance_viewerror', array('id' => $id), $options);
      $deleteUrl = Url::fromRoute('ada_compliance_delete', array('id' => $id));

      $rows[] = array(
        'data' => array(
          date("m/d/Y - H:i", strtotime($test_date)),
          t($short_details),
          t($short_details2),
          t($short_details3),
          t($short_details4),
          t($short_details5),
          t($short_details6),
          t($short_details7),
          \Drupal::l('View', $viewUrl),
          \Drupal::l('View', $viewUrl2),
          \Drupal::l('Delete', $deleteUrl)
        ),
      );

    }

    $table = array(
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#attributes' => array(
        'id' => 'ada-compliance-table',
      ),
    );
 
    return array(
      $text,
      $table,
    );

  }
}