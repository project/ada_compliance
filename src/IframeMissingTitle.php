<?php

/**
 * @file
 * Contains Drupal\ada_compliance\IframeMissingTitle.
 */

namespace Drupal\ada_compliance;

/**
 * Class IframeMissingTitle.
 *
 * @package Drupal\ada_compliance
 */

class IframeMissingTitle {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $iframetags = $dom->getElementsByTagName('iframe');
    $foundiframe_missing_title = 0;
    foreach ($iframetags as $iframe) {
      if (isset($iframe) and ($iframe->getAttribute('title') == "" or !$iframe->hasAttribute('title'))) {
        $filteredtitle = self::check_iframe_for_filtered_src_url($iframe->getAttribute('src'));
        $iframecode = $dom->saveXML($iframe, LIBXML_NOEMPTYTAG);
        if ($filteredtitle == 0 and !$foundiframe_missing_title) {		
          $result = $ErrorMessage::generateMessage($className, $iframecode, $num, $codes, $texts, $nid);
        }
      }
    }
    return $result;
  }

  static function check_iframe_for_filtered_src_url($inserturl, $iframecode = "") {
    if ($iframecode != "") {
      $dom = new \DOMDocument;
      libxml_use_internal_errors(true);
      $dom->loadHTML(htmlspecialchars_decode($iframecode));		
      $iframes = $dom->getElementsByTagName('iframe');
      foreach ($iframes as $iframe) {
        $inserturl = $iframe->getAttribute('src');	
      }
    }
    if($inserturl == "") return 0;
    $url = parse_url($inserturl);
    if (is_array($url) and array_key_exists('host',$url)) {
      switch ( str_replace( 'www.', '', $url['host'] ) ) {
        case 'cloudup.com': 
        case 'facebook.com':
        case 'flickr.com':
        case 'imgur.com':
        case 'instagram.com':	
        case 'issuu.com':
        case 'kickstarter.com': 
        case 'meetup.com':
        case 'mixcloud.com':
        case 'photobucket.com':	
        case 'reddit.com':	
        case 'speakerdeck.com':
        case 'scribd.com':	
        case 'slideshare.net':	
        case 'smugmug.com': 
        case 'tumbler.com':
        case 'twitter.com': 	
        case 'wordpress.org':	
        case 'polldaddy.com':
        case 'animoto.com':
        case 'blip.com':
        case 'collegehumor.com':
        case 'dailymotion.com':
        case 'funnyordie.com':
        case 'hulu.com':
        case 'ted.com':
        case 'videopress.com':
        case 'vimeo.com':
        case 'vine.com':
        case 'wordpress.tv':
        case 'youtube.com':
        case 'mixcloud.com':
        case 'reverbnation.com':
        case 'soundcloud.com':
        case 'spotify.com':
        case 'calendar.google.com':	
        return 1;
        break;
        default:
        return 0;
        break;
      }
    }
    return 0;	
  }
}