<?php

/**
 * @file
 * Contains Drupal\ada_compliance\ImgMissingAltMedia.
 */

namespace Drupal\ada_compliance;

/**
 * Class ImgMissingAltMedia.
 *
 * @package Drupal\ada_compliance
 */

class ImgMissingAltMedia {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $imgs = $dom->getElementsByTagName('img');
    $result = '';
    $foundTD = 0;
    foreach ($imgs as $img) {	
      $imgcode = $dom->saveXML($img, LIBXML_NOEMPTYTAG);
      if ($img != "" and (strpos($imgcode, "alt=")===false || strpos($imgcode, "alt=''")!==false || strpos($imgcode, "alt=\"\"")!==false) ) {	
        if (!$foundTD) {
          $foundTD = 1;
          $result = $ErrorMessage::generateMessage($className, $imgcode, $num, $codes, $texts, $nid);
        }
      }
    }
    return $result;
  }
}