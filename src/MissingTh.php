<?php

/**
 * @file
 * Contains Drupal\ada_compliance\MissingTh.
 */

namespace Drupal\ada_compliance;

/**
 * Class MissingTh.
 *
 * @package Drupal\ada_compliance
 */

class MissingTh {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $tables = $dom->getElementsByTagName('table');
    $foundTH = 0;
    foreach ($tables as $table) {
      $tablecode = $dom->saveXML($table, LIBXML_NOEMPTYTAG);
      if ($table != "" and (!stristr($tablecode, "<th") and !strstr($tablecode, "<td scope=") and $table->getAttribute('role') != "presentation")) {
        if (!$foundTH && !stristr($table->getAttribute('class'),"role-presentation")) {
          $result = $ErrorMessage::generateMessage($className, $tablecode, $num, $codes, $texts, $nid);
        }
      }
    }
    return $result;
  }
}