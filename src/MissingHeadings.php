<?php

/**
 * @file
 * Contains Drupal\ada_compliance\MissingHeadings.
 */

namespace Drupal\ada_compliance;

/**
 * Class MissingHeadings.
 *
 * @package Drupal\ada_compliance
 */

class MissingHeadings {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $h1 = $dom->getElementsByTagName('h1')->length;
    $h2 = $dom->getElementsByTagName('h2')->length;
    $h3 = $dom->getElementsByTagName('h3')->length;
    $h4 = $dom->getElementsByTagName('h4')->length;
    $h5 = $dom->getElementsByTagName('h5')->length;
    $h6 = $dom->getElementsByTagName('h6')->length;	
    $headings = ($h1+$h2+$h3+$h4+$h5+$h6);
    $foundIssue = 0;
    if ($headings == 0) {
      $errorcode = 'Missing headings';
      if (!$foundIssue) {
        $result = $ErrorMessage::generateMessage($className, $errorcode, $num, $codes, $texts, $nid);
      }
    }
    return $result;
  }
}