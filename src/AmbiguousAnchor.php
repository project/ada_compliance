<?php

/**
 * @file
 * Contains Drupal\ada_compliance\AmbiguousAnchor.
 */

namespace Drupal\ada_compliance;

/**
 * Class AmbiguousAnchor.
 *
 * @package Drupal\ada_compliance
 */

class AmbiguousAnchor {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = '';
    $content =  preg_replace("/&#?[a-z0-9]+;/i","",$content);
    $dom = new \DOMDocument;
    libxml_use_internal_errors(true);
    $dom->loadHTML($content);
    $links = $dom->getElementsByTagName('a');
    $foundambiguouslinktag = 0;
    foreach ($links as $link) {
      if (($link->getAttribute('class') =="" or 
      !strstr("no-title-convert",$link->getAttribute('class'))) and 
      $link->getAttribute('title') != "") {
        $linktext = $link->getAttribute('title');
      } elseif($link->getAttribute('aria-label') != "") {
        $linktext = $link->getAttribute('aria-label');
      } else {
        $linktext = trim($link->nodeValue);
      }
      $urlpattern = "#^(?i)\b((?:https?://(?:www\d{0,3}[.])?|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?������]))#";
      if (($linktext != "") and (
        preg_match('|^'.'continue reading'.'(\s)*$|i',$linktext) 
        or preg_match('|^'.'click here'.'(\s)*$|i',$linktext) 
        or preg_match('|^'.'Read More'.'(\s)*$|i',$linktext) 
        or preg_match('|^'.'Read More >'.'(\s)*$|i',$linktext) 
        or preg_match('|^'.'here'.'(\s)*$|i',$linktext) 
        or preg_match('|^'.'find out more'.'(\s)*$|i',$linktext) 
        or preg_match('|^'.'More...'.'(\s)*$|i',$linktext)
        or preg_match('|^'.'More'.'(\s)*$|i',$linktext)
        or preg_match('|^'.'clicking here'.'(\s)*$|i',$linktext)
        or preg_match('|^'.'website'.'(\s)*$|i',$linktext)
        or preg_match('|^'.'click for details'.'(\s)*$|i',$linktext) 
        or preg_match($urlpattern,$linktext))) {
        if (!$foundambiguouslinktag) {
          $ahtagcode = $dom->saveXML($link, LIBXML_NOEMPTYTAG);
          $result = $ErrorMessage::generateMessage($className, $ahtagcode, 
                    $num, $codes, $texts, $nid);
        }
      }
    }
    return $result;
  }
}