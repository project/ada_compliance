<?php

/**
 * @file
 * Contains Drupal\ada_compliance\ListIncorrectMarkup.
 */

namespace Drupal\ada_compliance;

/**
 * Class ListIncorrectMarkup.
 *
 * @package Drupal\ada_compliance
 */

class ListIncorrectMarkup {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $tags = array('p','h1','h2','h3','h4','h5','h6');		
    $founderror = 0;
    foreach ($tags as $tag) {		
      $elements = $dom->getElementsByTagName($tag);
      foreach ($elements as $element) {
        $pattern = '/^\s*\({0,1}(-|�|�|�|&#8226;|&bull;|&#8227;|&#9702;|&#8259;|&#8268;|&#8269;|&#8729;|\(1\)|1\.|1\)|2-|a\.|a\))(\w|\s|[[:punct:]]|[^<(\/>)]){2,}$/i';	
        $pattern2 = '/^a\.(\w\.)$/i';	
        $pattern3 = '/(<br\s*\/*>(<\/*[A-Z0-9]*\s*\/*>)*\s*)+\({0,1}(-|�|�|�|&#8226;|&bull;|&#8227;|&#9702;|&#8259;|&#8268;|&#8269;|&#8729;|1\.|1\)|2-|a\.|a\))(\w|\s|[[:punct:]]){2,}/i';	
        if (
        (preg_match($pattern,$element->nodeValue) and !preg_match($pattern2,$element->nodeValue))
        or (preg_match($pattern3,$ErrorMessage::GETinnerHTML($element)))) {
          $errorcode = $dom->saveXML($element, LIBXML_NOEMPTYTAG);
          if (!$founderror) {
            $result = $ErrorMessage::generateMessage($className, $errorcode, $num, $codes, $texts, $nid);
          }
        }
      }
    }
    return $result;
  }
}