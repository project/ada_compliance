<?php

/**
 * @file
 * Contains Drupal\ada_compliance\NestedFieldset.
 */

namespace Drupal\ada_compliance;

/**
 * Class NestedFieldset.
 *
 * @package Drupal\ada_compliance
 */

class NestedFieldset {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $found = 0;
    $result = "";
    $fieldsets = $dom->getElementsByTagName('fieldset');
    foreach ($fieldsets as $fieldset) {
      foreach ($fieldset->childNodes as $node) {
        if ($node->nodeName == "fieldset") {
          $errorcode = $dom->saveXML($fieldset, LIBXML_NOEMPTYTAG);
          $result = $ErrorMessage::generateMessage($className, $errorcode, $num, $codes, $texts, $nid);
        }
      }
      if ($found) break;
    }
    return $result;
  }
}