<?php

/**
 * @file
 * Contains Drupal\ada_compliance\RedundantAnchorText.
 */

namespace Drupal\ada_compliance;

/**
 * Class RedundantAnchorText.
 *
 * @package Drupal\ada_compliance
 */

class RedundantAnchorText {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $links = $dom->getElementsByTagName('a');
    $foundredundantlinktext = 0;
    $siteurl = $additionalInfo;
    $site_url_patterns[] =  '|'.$siteurl."|i";
    $site_url_patterns[] = '|'.preg_replace('#^[^\/]*#', '', preg_replace('#((https://)|http://)*#i','',$siteurl))."|i";
    foreach ($links as $link) {
      if ($link->nodeValue != "") {
        if (($link->getAttribute('class') =="" or !strstr("no-title-convert",$link->getAttribute('class'))) and $link->getAttribute('title') != "") {
          $linktext[] = $link->getAttribute('title');
        } else if ($link->getAttribute('aria-label') != "") {
          $linktext[] = $link->getAttribute('aria-label');
        } else {
          $linktext[] = strtolower($link->nodeValue);
        }
        $linkdestination[] = preg_replace($site_url_patterns, '',$link->getAttribute('href'));
      }
    }
    foreach ($links as $link) {
      $hrefmatchfound = 0;
      if ($link->nodeValue != "") {
        if (($link->getAttribute('class') =="" or !strstr("no-title-convert",$link->getAttribute('class'))) and $link->getAttribute('title') != "") {
          $linkmatch = array_keys($linktext, strtolower($link->getAttribute('title')));
        } else if ($link->getAttribute('aria-label') != "") {
          $linkmatch = array_keys($linktext, strtolower($link->getAttribute('aria-label')));
        } else {
          $linkmatch = array_keys($linktext, strtolower($link->nodeValue));
        }
        $destmatch = array_keys($linkdestination, preg_replace($site_url_patterns, '',$link->getAttribute('href')));
        if (sizeof($destmatch) < sizeof($linkmatch)) {
          $hrefmatchfound = 1;
        }
        if (sizeof($linkmatch) > 1 and $hrefmatchfound == 1) {
          $atagcode = $dom->saveXML($link, LIBXML_NOEMPTYTAG);
          if (!$foundredundantlinktext) {
            $result = $ErrorMessage::generateMessage($className, $atagcode, $num, $codes, $texts, $nid);
          }
        }
      }
    }
    return $result;
  }
}