<?php

/**
 * @file
 * Contains Drupal\ada_compliance\ImagemapMissingAlt.
 */

namespace Drupal\ada_compliance;

/**
 * Class ImagemapMissingAlt.
 *
 * @package Drupal\ada_compliance
 */

class ImagemapMissingAlt {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = '';
    $maps = $dom->getElementsByTagName('map');
    $k = 0;
    foreach ($maps as $map) {	
      $mapcode = $dom->saveXML($map, LIBXML_NOEMPTYTAG);
      $areas = $maps->item($k)->getElementsByTagName('area');
      $foundMap = 0;
      foreach ($areas as $area) {
        if (isset($area) and ($area->getAttribute('alt') == "")) {
          if (!$foundMap) {
            $foundMap = 1;
            $result = $ErrorMessage::generateMessage($className, $mapcode, $num, $codes, $texts, $nid);
          }
        }
      }
      $k++;			
    }
    return $result;
  }
}