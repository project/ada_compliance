<?php

/**
 * @file
 * Contains Drupal\ada_compliance\TabOrderModified.
 */

namespace Drupal\ada_compliance;

/**
 * Class TabOrderModified.
 *
 * @package Drupal\ada_compliance
 */

class TabOrderModified {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $errorfound = 0;
    $result = "";
    $tags = array('a','input','select','textarea','button','datalist', 'output', 'area');
    foreach ($tags as $tag) {
      $elements = $dom->getElementsByTagName($tag);
      foreach ($elements as $element) {
        if (isset($element) and $element->getAttribute('tabindex') > 0) {
          $errorcode = $dom->saveXML($element, LIBXML_NOEMPTYTAG);
          if (!$errorfound) {
            $result = $ErrorMessage::generateMessage($className, $errorcode, $num, $codes, $texts, $nid);
          }
        }
      }
      if ($errorfound) break;
    }
    return $result;
  }
}