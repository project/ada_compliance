<?php

/**
 * @file
 * Contains Drupal\ada_compliance\EmptyAnchor.
 */

namespace Drupal\ada_compliance;

/**
 * Class EmptyAnchor.
 *
 * @package Drupal\ada_compliance
 */

class EmptyAnchor {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = '';
    $foundemptylinktag = 0;
    $links = $dom->getElementsByTagName('a');
    foreach ($links as $link) {
      if (strip_tags($link->nodeValue) == "" and ($link->hasAttribute('href') and $link->getAttribute('aria-label') == "" and $link->getAttribute('title') == "")) {
        $atagcode = $dom->saveXML($link, LIBXML_NOEMPTYTAG);
        if (($atagcode != "") and (!strstr($atagcode, "id=") and !strstr($atagcode, "name=") and !strstr($atagcode, "\/>") and !preg_match('#<img(\S|\s)*alt=(\'|\")(\w)(\w|\s|\p{P}|\(|\)|\p{Sm}|~|`|\^|\$)+(\'|\")#',$atagcode) and !preg_match("#<i(\S|\s)*(title|aria-label)=('|\")\w(\w|\s|\p{P}|\(|\)|\p{Sm}|~|`|\^|\$)+('|\")+#",$atagcode))) {
          if (!$foundemptylinktag) {
            $result = $ErrorMessage::generateMessage($className, $atagcode, $num, $codes, $texts, $nid);
          }
        }
      }
    }
    return $result;
  }
}