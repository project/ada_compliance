<?php

/**
 * @file
 * Contains Drupal\ada_compliance\MissingOnkeypress.
 */

namespace Drupal\ada_compliance;

/**
 * Class MissingOnkeypress.
 *
 * @package Drupal\ada_compliance
 */

class MissingOnkeypress {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $elements = $dom->getElementsByTagName("*");
    $found = 0;
    foreach ($elements as $element) {
      if (isset($element)) {
        $founderror = 0;
        if (($element->getAttribute('onclick') or $element->getAttribute('ondblclick') 
        or $element->getAttribute('onmousedown') or $element->getAttribute('onmouseup'))
        and !$element->getAttribute('onkeypress') and !$element->getAttribute('onkeydown') and !$element->getAttribute('onkeyup')) $founderror = 1;
        if (($element->getAttribute('onmouseover') or $element->getAttribute('onmouseout') or $element->getAttribute('onmousemove'))
        and !$element->getAttribute('onfocus') and !$element->getAttribute('onblur')) $founderror = 1;
        if ($founderror == 1) {
          $code = $dom->saveXML($element, LIBXML_NOEMPTYTAG);
          if (!$found) {
            $result = $ErrorMessage::generateMessage($className, $code, $num, $codes, $texts, $nid);
          }
        }
      }
    }
    return $result;
  }
}