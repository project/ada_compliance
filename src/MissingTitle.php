<?php

/**
 * @file
 * Contains Drupal\ada_compliance\MissingTitle.
 */

namespace Drupal\ada_compliance;

/**
 * Class MissingTitle.
 *
 * @package Drupal\ada_compliance
 */

class MissingTitle {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    if (stristr($content, "<title>") && stristr($content, "</title>")) {
      $pos1 = strpos($content, "<title>")+7;
      $pos2 = strpos($content, "</title>", $pos1);
      $title = substr($content, $pos1, $pos2 - $pos1);
      if (!$title or stristr($title, 'untitled document')
      or stristr($title, 'enter the title of your html document here')
      or stristr($title, 'no title')
      or stristr($title, 'untitled page')
      or stristr($title, 'untitled')
      or stristr($title, '.html')
      or stristr($title, 'New Page')) {
        $result = $ErrorMessage::generateMessage($className, "<title>$title</title>", $num, $codes, $texts, $nid);
      }
    }
    return $result;
  }
}