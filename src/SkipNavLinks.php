<?php

/**
 * @file
 * Contains Drupal\ada_compliance\SkipNavLinks.
 */

namespace Drupal\ada_compliance;

/**
 * Class SkipNavLinks.
 *
 * @package Drupal\ada_compliance
 */

class SkipNavLinks {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $links = $dom->getElementsByTagName('a');
    $found = 0;
    foreach ($links as $link) {
      if (isset($link) and stristr($link->nodeValue, 'skip')) {
        $found = 1;
      }
    }
    if (!$found) {
      $result = $ErrorMessage::generateMessage($className, "Missing skip links.", $num, $codes, $texts, $nid);
    }
    return $result;
  }
}