<?php

/**
 * @file
 * Contains Drupal\ada_compliance\MissingAltTextAnchor.
 */

namespace Drupal\ada_compliance;

/**
 * Class MissingAltTextAnchor.
 *
 * @package Drupal\ada_compliance
 */

class MissingAltTextAnchor {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $links = $dom->getElementsByTagName('a');
    $foundemptylinktag = 0;
    foreach ($links as $link) {
      $images = $link->getElementsByTagName('img');
      if (isset($images)) {
        foreach ($images as $image) {
          $alt_text = $image->getAttribute('alt');
          if (isset($alt_text) and $alt_text == "" 
          and $link->hasAttribute('href')
          and (1  
          and ($link->getAttribute('aria-label') == "" 
          and $link->getAttribute('title') == ""))) {
            $atagcode = $dom->saveXML($link, LIBXML_NOEMPTYTAG);
            if (!$foundemptylinktag) {
              $result = $ErrorMessage::generateMessage($className, $atagcode, $num, $codes, $texts, $nid);
            }
          }
        }
      }
    }
    return $result;
  }
}