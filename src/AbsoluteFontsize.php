<?php

/**
 * @file
 * Contains Drupal\ada_compliance\AbsoluteFontsize.
 */

namespace Drupal\ada_compliance;

/**
 * Class AbsoluteFontsize.
 *
 * @package Drupal\ada_compliance
 */

class AbsoluteFontsize {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = '';
    $fontsearchpatterns[] = "|font\-size:\s?([\d]+)pt|i";
    $fontsearchpatterns[] = "|font\-size:\s?([\d]+)px|i";
    $fontsearchpatterns[] = "|font:\s?[\w\s\d*\s]*([\d]+)pt|i";
    $fontsearchpatterns[] = "|font:\s?[\w\s\d*\s]*([\d]+)px|i";
    $absolute_fontsizefound = 0;
    foreach ($fontsearchpatterns as $pattern) {
      if (preg_match_all($pattern, $content, $matches,PREG_PATTERN_ORDER)) {
        $matchsize = sizeof($matches);
        for ($i=0; $i < $matchsize; $i++) {
          if (isset($matches[0][$i]) and $matches[0][$i] != "") {
            $absolute_fontsize_errorcode = htmlspecialchars($matches[0][$i]);
            if (!$absolute_fontsizefound) {
              $result = $ErrorMessage::generateMessage($className, 
                        $absolute_fontsize_errorcode, $num, 
                        $codes, $texts, $nid);
            }
          }
        }
      }
    }
    return $result;
  }
}