<?php

/**
 * @file
 * Contains Drupal\ada_compliance\ImgLinkedToSelf.
 */

namespace Drupal\ada_compliance;

/**
 * Class ImgLinkedToSelf.
 *
 * @package Drupal\ada_compliance
 */

class ImgLinkedToSelf {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $links = $dom->getElementsByTagName('a');
    $foundemptylinktag = 0;
    foreach ($links as $link) {
      $images = $link->getElementsByTagName('img');
      if (isset($images)) {
        foreach ($images as $image) {
          $baseSRC = basename ($image->getAttribute('src'));
        }
        if (isset($baseSRC)) {
          if (basename($link->getAttribute('href')) == $baseSRC) {
            $atagcode = $dom->saveXML($link, LIBXML_NOEMPTYTAG);
            if (!$foundemptylinktag) {
              $result = $ErrorMessage::generateMessage($className, $atagcode, $num, $codes, $texts, $nid);
            }
          }
        }
      }
    }
    return $result;
  }
}