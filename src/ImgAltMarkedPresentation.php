<?php

/**
 * @file
 * Contains Drupal\ada_compliance\ImgAltMarkedPresentation.
 */

namespace Drupal\ada_compliance;

/**
 * Class ImgAltMarkedPresentation.
 *
 * @package Drupal\ada_compliance
 */

class ImgAltMarkedPresentation {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $images = $dom->getElementsByTagName('img');	
    $foundImage = 0;
    foreach ($images as $image) {
      if (isset($image) and $image->hasAttribute('alt') and $image->getAttribute('alt') != "" and $image->getAttribute('role') == "presentation") {
        $imagecode = $dom->saveXML($image, LIBXML_NOEMPTYTAG);
        if (!$foundImage) {
          $result = $ErrorMessage::generateMessage($className, $imagecode, $num, $codes, $texts, $nid);
        }
      }
    }
    return $result;
  }
}