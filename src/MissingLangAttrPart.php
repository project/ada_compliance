<?php

/**
 * @file
 * Contains Drupal\ada_compliance\MissingLangAttrPart.
 */

namespace Drupal\ada_compliance;

/**
 * Class MissingLangAttrPart.
 *
 * @package Drupal\ada_compliance
 */

class MissingLangAttrPart {

  /**
   * Get the result of checking page content against current ADA error.
   *
   * @param DOMDocument $dom
   * @param integer $num
   * @param array $codes
   * @param string $content
   * @param array $texts
   * @param Drupal\ada_compliance\ErrorMessage $ErrorMessage
   * @param string $className
   * @param string $additionalInfo
   * @param integer $nid
   *
   * @return string
   */
  static function check($dom, &$num, &$codes, 
                        $content, $texts, $ErrorMessage, $className, 
                        $additionalInfo, $nid) {
    $result = "";
    $html = $dom->getElementsByTagName('html');
    $pagelang = 'en';
    foreach ($html as $htmlcode) {
      if ($htmlcode->getAttribute('lang') != "") $pagelang = $htmlcode->getAttribute('lang');
      else if ($htmlcode->getAttribute('xml:lang') != "") $pagelang = $htmlcode->getAttribute('xml:lang');
    }
    if(!stristr($pagelang, "en")) return $result;
    $tags = array('p','h1','h2','h3','h4','h5','h6');
    $founderror = 0;
    foreach ($tags as $tag) {
      $elements = $dom->getElementsByTagName($tag);
      foreach ($elements as $element) {
        if ($element->getAttribute('lang') == "" and $element->getAttribute('xml:lang') == "") {
          $html = $ErrorMessage::GETinnerHTML($element);
          if (strip_tags($html) != "") {
            $lang = self::getTextLanguage($html, 'en');
            if ($lang != 'en') {
              if (!$founderror) {
                $result = $ErrorMessage::generateMessage($className, $tablecode, $num, $codes, $texts, $nid);
              }
            }
          }
        }
      }
    }
    return $result;
  }

  static function getTextLanguage($text, $default) {
    $text = strip_tags($text);
    $supported_languages = array('en','de','es','fr');
    $wordList['en'] = array ('the', 'be', 'to', 'of', 'and', 'a', 'in', 'that', 'have', 'I', 'it', 'for', 'not', 'on', 'with', 'he', 
    'as', 'you', 'do', 'at', 'no', 'me', 'ha', 'sin', 'entre', 'this', 'but', 'his','by','from','they','we','say','her','she','or','an','will', 'my','one','all', 'would', 'there', 'their','what', 'so', 'up', 'out', 'if','about', 'who','get','which','go', 'me', 'when','make','can','like', 'time','no', 'just','him','know','take','people','into','year' ,'your','good','some','could','them', 'see', 'other', 'than');
    $text = preg_replace("/[^[]_A-Za-z]/", ' ', $text);
    foreach ($supported_languages as $language) {
      $counter[$language]=0;
    }
    for ($i = 0; $i < 76; $i++) {
      foreach ($supported_languages as $language) {
        if (array_key_exists($i,$wordList[$language]))
          $counter[$language] = $counter[$language] + substr_count($text, ' ' .$wordList[$language][$i] . ' ');
      }
    }
    $max = max($counter);
    $maxs = array_keys($counter, $max);
    if (count($maxs) == 1) {
      $winner = $maxs[0];
      $second = 0;
      foreach ($supported_languages as $language) {
        if ($language <> $winner) {
          if ($counter[$language]>$second) {
            $second = $counter[$language];
          }
        }
      }
      $percent = ($second / $max);
      if ($percent > 0.1) {
        return $winner;
      }
    }
    return $default;
  }
}