;(function ($, window, document, undefined) {

    $.fn.myAjaxCallback = function(error, nid) {
       console.log(error);
       console.log(nid);
       //set some input field's value to 'My arguments'
       $('#error_' + error).html(nid);
    };

    $(document).ready(function() {

        $('a').each(function() {
		if ($(this).text()=='Ada compliance') {
			$(this).text('ADA compliance');
		}
        });

        $( ".error_list" ).wrap( "<div class='error_list-wrapper'></div>" );

        $('.code_without_number').on('click', function() {
            $(this).find('div').toggleClass('show');
        });

	if ($('.checkallpages').length) {

                var classes = ['ada_message_page_normal', 'ada_message_page_alert', 'ada_message_page_warning'];
		$('.page-checkbox').each(function() {
			var id = $(this).attr('id');
                        for (var i=0; i<3; i++) {
				if ($(this).hasClass(classes[i])) {
					$('label[for="'+id+'"]').addClass(classes[i]);
					$(this).removeClass(classes[i]);
				}
			}
		});

		$('#edit-allcontenttypes').on('click', function(){
			var checked = $(this).prop('checked');
                        $('input[type="checkbox"]').each(function() {
                                if ($(this).attr('id').indexOf('edit-options')==-1) {
                                     $(this).attr('checked', checked);
                                }
                        });
		});

                $('details').wrap('<div class="ada_content_type_wrap" />');

		$('details').each(function() {
			var node_type = $(this).attr('id').replace('edit-detailspages', '');
			$('.form-item-detailspages'+node_type+'-checkallpages').insertBefore( $(this) );
		});

		$('.checkallpages').on('click', function() {
			var checked = $(this).prop('checked');
			var node_type = $(this).attr('id').replace('edit-detailspages', '').replace('-checkallpages', '').replace(/-/g, '_');
                        $('input[type="checkbox"]').each(function() {
                                if ($(this).hasClass('page-'+node_type) || $(this).hasClass('page-'+node_type+'_')) {
                                     $(this).attr('checked', checked);
                                }
                        });
		});
		$('#ada-compliance-add').on("submit", function(){
			$(this).find("#edit-actions-submit").after("<span style='padding-left: 20px;'>Tests launched...</span>")
		})
	}
	/*
	if ($('#edit-detailsoptions-checkall').length) {
		$('#edit-detailsoptions-checkall').on('click', function() {
			var checked = $(this).prop('checked');
                        $('#edit-detailsoptions-level1-details1').prop('checked', checked);
                        $('#edit-detailsoptions-level2-details2').prop('checked', checked);
                        $('#edit-detailsoptions-level3-details3').prop('checked', checked);
			$('input[type="checkbox"]').each(function() {
				if ($(this).attr('name').indexOf('check')!=-1) {
					$(this).attr('checked', checked);
				}
			});
	        });
                for(var i=1; i <= 3; i++) {
                    $('#edit-detailsoptions-level'+i+'-details'+i).on('click', { ind: i}, function(event) {
                        var checked = $(this).prop('checked');
                        var ind = event.data.ind;
                        $('input[type="checkbox"]').each(function() {
                                if ($(this).hasClass('details'+ind)) {
                                     $(this).attr('checked', checked);
                                }
                        });
                    });
                }
		$('#edit-detailspages-checkallpages').on('click', function() {
			var checked = $(this).prop('checked');
			$('input[type="checkbox"]').each(function() {
				if ($(this).attr('name').indexOf('page')!=-1) {
					$(this).attr('checked', checked);
				}
			});
	        });
	}
	*/
        if ($('.link_to_error').length) {
		$('.link_to_error').on('click', function() {
                        $('.nid-'+$(this).data('nid')).css('display', 'block');
			// $('a[name="report-'+$(this).data('nid')+'"]').parent().next().css('display', 'block');
			$('a[name="report-'+$(this).data('nid')+'"]').text('[ Hide issues in code ]');
		});
        }

	if ($('.open_rest_errors').length) {
		$('.open_rest_errors').on('click', function() {
			var display_mode = $(this).next().css('display');
            var panel = $(this).closest('.panel');
            var errorListWrapper = $(this).closest('.error_list-wrapper');
			console.log(display_mode);
			if (display_mode == 'none') {
				$(this).next().show();
                panel.height(errorListWrapper.height());
			} else {
				$(this).next().hide();
                panel.height(errorListWrapper.height());
			}
		});
	}

	if ($('#edit-actions-submit').length && $('#edit-actions-submit').val()=='Launch tests') {
		$('#edit-actions-submit').insertBefore( 'label[for="edit-detailsoptions"]' );
	}

	if (location.href.indexOf('/ada_compliance/view/')!=-1) {
		$('input[value="Filter"]').on('click', function () {
			var params = 'filter=1';
			if ($('input[name="multipageissues"]:checked').length > 0) params += (params ? '&' : '') + 'multipage=1';
			if ($('input[name="pagespecificerrors"]:checked').length > 0) params += (params ? '&' : '') + 'errors=1';
			if ($('input[name="pagespecificwarnings"]:checked').length > 0) params += (params ? '&' : '') + 'warnings=1';
			if (params) params = '?' + params;
			location.href = location.origin + location.pathname + params;
		});
		if(!$('input[name="multipageissues"]').prop('checked')) {
			$('button:contains("Multi-page issues")').each(function() {
				$(this).next().hide();
				$(this).hide();
			});
		}
		if(!$('input[name="pagespecificerrors"]').prop('checked')) {
			$('button:contains("Page-specific errors")').each(function() {
			console.log('Not show error1!');
				$(this).next().hide();
				$(this).hide();
			});
		}
		if(!$('input[name="pagespecificwarnings"]').prop('checked')) {
			$('button:contains("Page-specific warnings")').each(function() {
				$(this).next().hide();
				$(this).hide();
			});
		}
	}

        if ($('.show-report').length) {
            $('.show-report').on('click', function() {
		var nid = $(this).attr('name').replace('report-','');
                var displayMode = $('.nid-'+nid).css('display');
                if (displayMode=='none') {
                    $('.nid-'+nid).css('display', 'block');
                    $(this).text('[ Hide issues in code ]');
                } else {
                    $('.nid-'+nid).css('display', 'none');
                    $(this).text('[ Show issues in code ]');
                }
            });
        }

	if ( !$('.error_num').length) {

		$('.error_num').on('change', function() {
			var report_link = 'report-' + $(this).attr('name').replace('errorsfornode-','');
			console.log(report_link);
			$('a[name="' + report_link + '"]').parent().next().css('display', 'block');
			$('a[name="' + report_link + '"]').text('[ Hide issues in code ]');
			window.location.hash = '#' + $(this).val();
		});

                var adminPanelHeight = document.getElementById('toolbar-bar').clientHeight + document.getElementById('toolbar-item-administration-tray').clientHeight;
                var stickyMaxHeight = window.innerHeight - adminPanelHeight;

                Array.prototype.slice.call(document.querySelectorAll('table.ada-table div.stickyDa')).forEach(function(a) {
                    var b = null, P = adminPanelHeight;
                    window.addEventListener('scroll', Ascroll, false);
                    document.body.addEventListener('scroll', Ascroll, false);
                    if (b == null) {
                        var Sa = getComputedStyle(a, ''), s = '';
                        for (var i = 0; i < Sa.length; i++) {
                            if (Sa[i].indexOf('padding') == 0 || Sa[i].indexOf('border') == 0 || Sa[i].indexOf('outline') == 0 || Sa[i].indexOf('box-shadow') == 0 || Sa[i].indexOf('background') == 0) {
                                s += Sa[i] + ': ' +Sa.getPropertyValue(Sa[i]) + '; '
                            }
                        }
                        b = document.createElement('div');
                        b.style.cssText = s + ' box-sizing: border-box; width: ' + a.offsetWidth + 'px; max-height: ' + stickyMaxHeight + 'px;';
                        a.insertBefore(b, a.firstChild);
                        var l = a.childNodes.length;
                        for (var i = 1; i < l; i++) {
                            b.appendChild(a.childNodes[1]);
                        }
                        a.style.minHeight = b.getBoundingClientRect().height + 'px';
                        a.style.padding = '0';
                        a.style.border = '0';
                    }
                    function Ascroll() {

                        var Ra = a.getBoundingClientRect(),
                            R = Math.round(Ra.top + b.getBoundingClientRect().height  - a.parentNode.getBoundingClientRect().bottom + parseFloat(getComputedStyle(a.parentNode, '').paddingBottom.slice(0, -2)));
                        if ((Ra.top - P) <= 0) {
                            if ((Ra.top - P) <= R) {
                                b.className = 'stop';
                                b.style.top = - R +'px';
                            } else {
                                b.className = 'sticky';
                                b.style.top = P +'px';
                            }
                        } else {
                            b.className = '';
                            b.style.top = '';
                        }
                        window.addEventListener('resize', function() {
                            a.children[0].style.width = getComputedStyle(a, '').width
                        }, false);
                    }                   
                });


                var acc = document.getElementsByClassName("accordion");
                var i;

                for (i = 0; i < acc.length; i++) {
                    acc[i].addEventListener("click", function() {
                        this.classList.toggle("active");
                        var panel = this.nextElementSibling;
                        if (panel.style.minHeight) {
                            panel.style.minHeight = null;
                            panel.style.height = 0;
                        } else {
                            panel.style.minHeight = panel.scrollHeight + "px";
                        }
                    });
                }

	}

        /**
         * Check a href for an anchor. If exists, and in document, scroll to it.
         * If href argument ommited, assumes context (this) is HTML Element,
         * which will be the case when invoked by jQuery after an event
         */
        function scroll_if_anchor(href) {
            href = typeof(href) == "string" ? href : $(this).attr("href");

            // You could easily calculate this dynamically if you prefer
            var fromTop = adminPanelHeight;

            // If our Href points to a valid, non-empty anchor, and is on the same page (e.g. #foo)
            // Legacy jQuery and IE7 may have issues: http://stackoverflow.com/q/1593174
            if(href.indexOf("#") == 0) {
                var $target = $(href);

                // Older browser without pushState might flicker here, as they momentarily
                // jump to the wrong position (IE < 10)
                if($target.length) {
                    $('html, body').animate({ scrollTop: $target.offset().top - fromTop });
                    if(history && "pushState" in history) {
                        history.pushState({}, document.title, window.location.pathname + href);
                        return false;
                    }
                }
            }
        }

// When our page loads, check to see if it contains and anchor
        scroll_if_anchor(window.location.hash);

// Intercept all anchor clicks
        $("body").on("click", "a", scroll_if_anchor);

        //create scroll-to-top button
        document.body.insertAdjacentHTML('afterBegin', '<button id="scroll-top" title="Go to top">Top</button>');

        $('#scroll-top').click(topFunction);
        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function() {scrollFunction()};

        function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                document.getElementById("scroll-top").style.display = "block";
            } else {
                document.getElementById("scroll-top").style.display = "none";
            }
        }

// When the user clicks on the button, scroll to the top of the document
        function topFunction() {
            $('html, body').animate({ scrollTop: 0});
        }
    });

}(jQuery, window, document));